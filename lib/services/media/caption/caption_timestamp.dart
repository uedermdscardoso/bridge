
class CaptionTimestamp {

  double startTime;
  double endTime;
  String word;

  CaptionTimestamp({ required this.startTime, required this.endTime, required this.word });

  CaptionTimestamp.fromJson(Map<String, dynamic> json)
      : startTime = json['startTime'],
        endTime = json['endTime'],
        word = json['word'];

  void setWord({ required String word }) => this.word = word;

}