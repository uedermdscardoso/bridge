
import 'dart:io';

import 'package:flutter_ffmpeg/flutter_ffmpeg.dart';
import 'package:bridge/domain/model/user/users.dart';
import 'package:path/path.dart';
import 'package:path_provider/path_provider.dart';
import 'package:sqflite/sqflite.dart';
import 'package:uuid/uuid.dart';

class MediaService {

  Future<File> changeSpeed({ required double speed, required File audio }) async {
    try {
      FlutterFFmpeg flutterFFmpeg = FlutterFFmpeg();
      final newPath = join((await getTemporaryDirectory()).path, '${ Uuid().v4() }.aac');

      String syntax = '-i ${ audio.path } -filter:a "atempo=${ speed }" -vn ${ newPath }';

      await flutterFFmpeg.execute(syntax)
          .catchError((e) => print(e));

      return File(newPath);

    } catch(e) {
      throw Exception(e);
    }
  }

  Future<File> concatTwoAudios({ required File input1, required File input2 }) async {
    FlutterFFmpeg flutterFFmpeg = FlutterFFmpeg();
    final newPath = join((await getTemporaryDirectory()).path, '${ Uuid().v4() }.aac');

    final String syntax = '-i ${ input1.path } -i ${ input2.path } -filter_complex "[0:0][1:0]concat=n=2:v=0:a=1[out]" -map "[out]" ${ newPath }';

    await flutterFFmpeg.execute(syntax)
        .catchError((e) => print(e));

    return File(newPath);
  }

  Future<File> mergeAudios({ required File first, required File second }) async {
    try {
      FlutterFFmpeg flutterFFmpeg = FlutterFFmpeg();
      final newPath = join((await getTemporaryDirectory()).path, '${ Uuid().v4() }.aac');

      String syntax = '-i ${ first.path } -i ${ second.path } -filter_complex amerge=inputs=2 -ac 2 ${ newPath }';

      await flutterFFmpeg.execute(syntax)
          .catchError((e) => print(e));

      return File(newPath);

    } catch(e) {
      throw Exception(e);
    }
  }

  Future<File> applyVolume({ required String filepath, required double volume }) async {
    try {
      FlutterFFmpeg flutterFFmpeg = FlutterFFmpeg();
      final newPath = join((await getTemporaryDirectory()).path, '${ Uuid().v4() }.aac');

      //filter:a
      String syntax = '-i $filepath -af "volume=$volume" $newPath';

      await flutterFFmpeg.execute(syntax)
          .catchError((e) => print(e));

      return File(newPath);
    } catch(e) {
      throw Exception(e);
    }
  }

  Future<File> cutAudioInterval({ required String path, required String initialInterval, required double seconds }) async {
    try {
      final FlutterFFmpeg flutterFFmpeg = FlutterFFmpeg();
      final newPath = join((await getTemporaryDirectory()).path, '${ Uuid().v4() }.aac');

      final String syntax = "-ss ${ initialInterval } -t ${ seconds } -i ${ path } ${ newPath }";

      await flutterFFmpeg.execute(syntax)
          .catchError((e) => print(e));

      return File(newPath);
    } catch (e) {
      throw Exception(e);
    }
  }

  Future<File> cutAudio({ required double seconds, required String path }) async {
    try {
      FlutterFFmpeg flutterFFmpeg = FlutterFFmpeg();
      final newPath = join((await getTemporaryDirectory()).path, '${ Uuid().v4() }.aac');

      String syntax = "-i ${ path } -ss 00:00:00 -t ${ seconds+1 } -c copy ${ newPath }";

      //ffmpeg -i Funny.mkv -ss 00:00:20 -t 15 -c copy Funny_cut.mp3
      await flutterFFmpeg.execute(syntax)
          .catchError((e) => print(e));

      return File(newPath);
    } catch(e) {
      throw Exception(e);
    }
  }

  Future<File> addEffect({ required String syntax, required File audio }) async {
    try {
      FlutterFFmpeg flutterFFmpeg = FlutterFFmpeg();
      final newPath = join((await getTemporaryDirectory()).path, '${ Uuid().v4() }.aac');

      String input = '{input}';
      String output = '{output}';

      if(syntax.isNotEmpty && (syntax.contains(input) && syntax.contains(output))) {
        syntax = syntax.replaceFirst(input, audio.path);
        syntax = syntax.replaceFirst(output, newPath);

        //"-i {input} -filter_complex 'sofalizer=sofa=temp/hrtf c_nh877.sofa:type=freq:radius=1' {output}";

        await flutterFFmpeg.execute(syntax)
            .catchError((e) => print(e));
      }

      return File(newPath);
    } catch(e) {
      throw Exception(e);
    }
  }

  /* DATABASE */
  Future<Database> createMediaViewTable() async {
    try{
      return openDatabase(
        join(await getDatabasesPath(), 'musly_db.db'),
        onCreate: (db, version) {
          return db.execute(
            'CREATE TABLE media_view (media_id TEXT PRIMARY KEY, user_id TEXT)',
          );
        },
        version: 1,
      );
    } catch(e) {
      throw Exception(e);
    }
  }

  Future<List<String>> getLocalMediaViewByUser({ required Users currentUser }) async {
    try {
      final Database db = await openDatabase(join(await getDatabasesPath(), 'musly_db.db'));
      List<Map<String, dynamic>> maps = await db.rawQuery('SELECT media_id FROM media_view WHERE user_id=?', [ currentUser.id ]);
      List<String> mediaId = [];

      if(maps != null && maps.isNotEmpty) {
        maps.forEach((map) {
          mediaId.add(map['media_id']);
        });

        return mediaId;
      }

      return List<String>.of({});
    } catch(e) {
      throw Exception(e);
    }
  }
}
