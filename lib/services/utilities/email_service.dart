
import 'package:mailer/mailer.dart';
import 'package:mailer/smtp_server.dart';

class EmailService {

  static final EMAIL_SERVER = 'infinity.app@gmail.com';
  static final PASSWORD_SERVER = 'wT3jk06G&Lt%';

  static String obscureText({ required int limit, required String text}){
    for(int i=limit; i<text.length; i++) {
      text = text.replaceRange(i, i+1, '*');
    }
    return text;
  }

  static String obscureEmail({ required String email}){
    int pos = email.indexOf('@');
    String first = email.substring(0, pos);
    if(first.length > 3)
      for(int i=1; i<pos-1; i++) {
        email = email.replaceRange(i, i+1, '*');
      }
    else {
      email = email.replaceRange(0, pos, '*');
    }
    return email;
  }

  static Future<bool> sendEmail({ required String title, required String recipient, required String template }) async {
    final smtpServer = gmail(EMAIL_SERVER, PASSWORD_SERVER);
    bool sent = false;
    final message = Message()
      ..from = Address(EMAIL_SERVER, 'Musly App')
      ..recipients.add(recipient)
      ..subject = title
      ..html = template;
    try {
      final sendReport = await send(message, smtpServer);
      sent = true;
    } on MailerException catch (e) {
      sent = false;
      throw Exception(e);
    }
    return sent;
  }

  /*static Future<String> sendEmail({ BuildContext context, String subject, String body, List<String> recipients, List<String> attachments, bool isHTML }) async {
    final MailOptions mailOptions = MailOptions(
      body: body,
      subject: subject,
      //recipients: recipients,
      isHTML: isHTML,
      ccRecipients: <String>['no-reply@helpmeapp.com'],
      //attachments: attachments,
    );

    String platformResponse = '';

    try {
      Transintl = Translate.of(context);

      final MailerResponse response = await FlutterMailer.send(mailOptions);

      switch (response) {
        case MailerResponse.saved:
          platformResponse = intl.translate('email.savedToDraft');
          break;
        case MailerResponse.sent:
          platformResponse = intl.translate('email.sent');
          break;
        case MailerResponse.cancelled:
          platformResponse = intl.translate('email.cancelled');
          break;
        case MailerResponse.android:
          platformResponse = intl.translate('email.success');
          break;
        default:
          platformResponse = intl.translate('email.unknown');
          break;
      }
    } on PlatformException catch (error) {
      platformResponse = error.toString();
    } catch (error) {
      platformResponse = error.toString();
    }

    return platformResponse;
  }*/

}