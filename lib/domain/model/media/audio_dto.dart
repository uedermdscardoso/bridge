
import 'dart:io';

import 'package:bridge/domain/model/media/effect.dart';
import 'package:bridge/services/media/media_service.dart';

class AudioDTO {

  late File audio;

  AudioDTO({ required this.audio });

  Future<void> applyEffect({ required Effect effect }) async {
    if(!effect.unavailable! && this.audio.existsSync()) {
      final MediaService mediaService = MediaService();

      this.audio = await mediaService.addEffect(syntax: effect.syntax!, audio: this.audio);
    }
  }

  Future<void> applySpeed({ required double speed }) async {
    if(this.audio.existsSync() && speed != 1.0) {
      final MediaService mediaService = MediaService();

      this.audio = await mediaService.changeSpeed(speed: speed, audio: this.audio);
    }
  }

  Future<void> stitchAudio({ required File clipped }) async {
    if(this.audio.existsSync() && clipped.existsSync()) {
      final MediaService mediaService = MediaService();

      this.audio = await mediaService.concatTwoAudios(input1: clipped, input2: this.audio);
    }
  }
}