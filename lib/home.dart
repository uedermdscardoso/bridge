
import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:bridge/Translate.dart';
import 'package:bridge/main.dart';
import 'package:bridge/services/auth/authentication_service.dart';
import 'package:bridge/services/notification/notification_service.dart';
import 'package:bridge/services/user/user_service.dart';
import 'package:bridge/services/utilities/util_service.dart';
import 'package:bridge/shared/profile_photo.dart';
import 'package:bridge/widgets/chat/conversation_group.dart';
import 'package:bridge/widgets/followers_media_page.dart';
import 'package:bridge/widgets/studio/recording_studio.dart';
import 'package:bridge/widgets/recommended_media_page.dart';
import 'package:bridge/widgets/item_menu_principal.dart';
import 'package:bridge/widgets/notification/notification_screen.dart';
import 'package:bridge/widgets/auth/anonymous_profile_screen.dart';
import 'package:bridge/widgets/profile/profile_details.dart';
import 'package:bridge/widgets/search/search_screen.dart';
import 'domain/model/user/users.dart';
import 'package:flutter/services.dart';
import 'package:cloud_firestore/cloud_firestore.dart';

class Home extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => _HomeState();
}

class _HomeState extends State<Home> with AutomaticKeepAliveClientMixin {

  final _notificationService = NotificationService();
  final _authService = AuthenticationService();
  final UserService _userService = UserService();

  PageController _pageController = PageController(initialPage: 0);
  late Users _currentUser;

  int _total = 0;
  String _totalFormated = '';
  bool _isForYou = true;
  bool _isHome = true;
  String _currentScreen = 'home';

  late Translate _intl;

  @override
  void initState() {
    _init();
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
  }

  _init() {
    mainStore.notification.addNewPersonalNotifications();
    mainStore.auth.addCurrentUser();
  }

  @override
  Widget build(BuildContext context) {

    _intl = Translate.of(context);

    return AnnotatedRegion<SystemUiOverlayStyle>(
      value: const SystemUiOverlayStyle(
        statusBarColor: Colors.transparent,
        statusBarBrightness: Brightness.light,
        statusBarIconBrightness: Brightness.light,
        systemNavigationBarColor: Colors.black,
        systemNavigationBarIconBrightness: Brightness.light,
        systemNavigationBarDividerColor: Colors.black,
      ),
      child: Material(
        color: Colors.white,
        child: Container(
          decoration: const BoxDecoration(
            gradient: LinearGradient(
              begin: Alignment.topRight,
              end: Alignment.bottomLeft,
              colors: [
                Color.fromRGBO(92, 92, 92, 1),
                Colors.black,
              ],
            ),
          ),
          child: Observer(
            builder: (_) {

              _isHome = mainStore.isHome;
              _currentUser = mainStore.auth.currentUser;

              return Stack(
                children: [
                  _getScreen(type: _currentScreen),
                  Visibility(
                    visible: _currentScreen.compareTo('home') == 0,
                    child: Padding(
                      padding: const EdgeInsets.only(top: 55, right: 8, left: 25), //top: 60
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                          Text(_isForYou ? "Bridge" : _intl.translate('SCREEN.HOME.PAGES.FOLLOWING.TITLE'),
                            style: const TextStyle(fontFamily: 'Arvo', color: Colors.white, fontWeight: FontWeight.bold, fontSize: 21)),
                          Row(
                            children: [
                              GestureDetector(
                                onTap: () => setState(() {
                                  if(!_isForYou)
                                    _pageController.jumpToPage(0);
                                  else
                                    _pageController.jumpToPage(1);

                                  _isForYou = !_isForYou;
                                }),
                                child: Stack(
                                  children: [
                                    Container(
                                      padding: const EdgeInsets.all(8.0),
                                      child: Icon(Icons.group, color: _isForYou ? Colors.grey : Colors.white, size: 30),
                                    ),
                                    Visibility(
                                      visible: _isForYou,
                                      child: Positioned(
                                        top: 24,
                                        left: 2,
                                        child: RotationTransition(
                                          turns: new AlwaysStoppedAnimation(50 / 360),
                                          child: Container(
                                            width: 40,
                                            height: 2,
                                            decoration: BoxDecoration(
                                              color: Colors.grey[200],
                                              borderRadius: BorderRadius.circular(12),
                                            ),
                                          ),
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                              Stack(
                                children: [
                                  ItemMenuPrincipal(
                                      onTap: () async {
                                        if(await _authService.isAnonymous())
                                          Navigator.of(context).push(UtilService.createRoute(AnonymousProfileScreen()));
                                        else
                                          Navigator.of(context).push(UtilService.createRoute(NotificationScreen()));
                                      },
                                      isHome: _isHome,
                                      icon: Icons.notifications,
                                      text: 'Notifications'
                                  ),
                                  StreamBuilder(
                                    stream: mainStore.notification.newPersonalNotifications,
                                    builder: (context, snapshot) {

                                      if(snapshot.hasData) {

                                        final QuerySnapshot snap = snapshot.data as QuerySnapshot;

                                        _total = snap.docs.length;
                                      }

                                      _totalFormated = _notificationService.formatNewNotifitionsCount(total: _total);

                                      return Visibility(
                                        visible: _total > 0,
                                        child: Positioned(
                                          top: 6,
                                          left: 20,
                                          child: Container(
                                            width: 20,
                                            height: 20,
                                            decoration: BoxDecoration(
                                              color: Colors.red,
                                              borderRadius: BorderRadius.circular(100),
                                            ),
                                            child: Center(
                                                child: Text('${ _totalFormated }', style: TextStyle(color: Colors.white, fontSize: 10, decoration: TextDecoration.none))
                                            ),
                                          ),
                                        ),
                                      );
                                    },
                                  ),
                                ],
                              ),
                            ],
                          ),
                        ],
                      ),
                    ),
                  ),
                  Align( //Menu Principal
                    alignment: Alignment.bottomCenter,
                    child: Container(
                      padding: const EdgeInsets.only(top: 8, left: 20, right: 30),
                      height: 60,
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          GestureDetector(
                            onTap: () async {
                              if(await _authService.isAnonymous()) {
                                Navigator.of(context).push(UtilService.createRoute(AnonymousProfileScreen()));
                              } else {
                                Navigator.of(context).push(UtilService.createRoute(
                                  const RecordingStudio(), //Para a criação de áudios
                                ));
                              }
                            },
                            child: Container(
                              width: 55,
                              height: 35,
                              decoration: BoxDecoration(
                                color: !_isHome ? Colors.black : Colors.white,
                                borderRadius: BorderRadius.circular(100),
                              ),
                              child: Icon(Icons.add, color: _isHome ? Colors.black : Colors.white, size: 22),
                            ),
                          ),
                          ItemMenuPrincipal(
                            onTap: () {
                              mainStore.changeIsHome(isHome: true);
                              setState(() => {
                                _isForYou = true,
                                _currentScreen = 'home',
                                _pageController.jumpToPage(0),
                              });
                            },
                            customIcon: RotationTransition(
                              turns: new AlwaysStoppedAnimation(40 / 360),
                              child: Icon(Icons.all_inclusive,
                                color: _isHome ? Colors.white : Colors.black,
                                size: 28,
                              ),
                            ),
                            //icon: Icons.all_inclusive, //home
                            isHome: _isHome,
                            text: 'Home',
                          ),
                          ItemMenuPrincipal(
                              onTap: () {
                                mainStore.changeIsHome(isHome: false);
                                setState(() => _currentScreen = 'search');
                              },
                              icon: Icons.search,
                              isHome: _isHome, //_currentScreen.compareTo('home') == 0 ? Colors.white70 : Colors.black,
                              text: 'Discover'
                          ),
                          ItemMenuPrincipal(
                            onTap: () async {
                              if(await _authService.isConnected()) {
                                mainStore.changeIsHome(isHome: false);
                                setState(() => _currentScreen = 'directMessage');
                              } else
                                UtilService.notify(context: context, message: '${ _intl.translate('CONNECTION.INTERNET.FAILED') }');
                            },
                            isHome: _isHome,
                            customIcon: RotationTransition(
                              turns: new AlwaysStoppedAnimation(300 / 360),
                              child: Icon(Icons.send_outlined,
                                color: this._isHome ? Colors.white : Colors.black,
                                size: 28,
                              ),
                            ),
                          ),
                          GestureDetector(
                            onTap: () async {
                              if(await _authService.isAnonymous())
                                Navigator.of(context).push(UtilService.createRoute(AnonymousProfileScreen()));
                              else
                                Navigator.of(context).push(UtilService.createRoute(ProfileDetails(isCurrentUser: true)));
                            },
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                ProfilePhoto(
                                  width: 35,
                                  height: 35, //28
                                  user: _currentUser,
                                ),
                              ],
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                ],
              );
            },
          ),
        ),
      ),
    );
  }

  _changePage({ required bool isForYou }){
    if(!isForYou)
      _pageController = PageController(initialPage: 1);
    else
      _pageController = PageController(initialPage: 0);
  }

  Widget _getScreen({ required String type }) {

    switch(type) {
      case 'home':
       _changePage(isForYou: _isForYou);
       return _createPageView();
      case 'search':
        return SearchScreen();
      case 'directMessage': {
        return ConversationGroup();
      }
      default:
        return _createPageView();
    }
  }

  Widget _createPageView() {
    return PageView(
      key: Key('page'),
      physics: NeverScrollableScrollPhysics(),
      controller: _pageController,
      allowImplicitScrolling: false,
      scrollDirection: Axis.horizontal,
      children: <Widget>[
        RecommendedMediaPage(),
        FollowersMediaPage(),
      ],
    );
  }

  @override
  bool get wantKeepAlive => true;

}
