
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:bridge/domain/model/user/users.dart';
import 'package:bridge/shared/profile_button.dart';

class ProfilePhoto extends StatelessWidget {

  Users user;
  double width;
  double height;
  double _borderRadius = 0;

  ProfilePhoto({Key? key, required this.user, this.width : 70, this.height : 70 }) : super(key: key);

  @override
  Widget build(BuildContext context) {

    _borderRadius = 15 * (this.width / 50);

    return Container(
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(_borderRadius), //15
        color: Color.fromRGBO(240, 240, 240, 1),
        border: Border.all(width: 0.5, color: Color(int.parse("FFFFDFE0", radix: 16))),
      ),
      child: ClipRRect(
        borderRadius: BorderRadius.circular(_borderRadius),
        child: CachedNetworkImage(
          imageUrl: user != null ? user.photo : '',
          width: width, //70
          height: height, //70
          fit: BoxFit.cover,
          placeholder: (context, url) => _createIcon(currentUser: this.user),
          errorWidget: (context, url, error) => _createIcon(currentUser: this.user),
        ),
      ),
    );

  }

  Widget _createIcon({ required Users currentUser }) {

    if(currentUser == null || currentUser.photo == null || currentUser.photo.isEmpty)
      return Image.asset('assets/icons/profile.png');

    final double letterSizeDefault = 35 * 0.8; //35
    final int defaultSize = 70;

    final String firstLetter = currentUser.firstName.substring(0, 1);
    final double fontSize = (this.width * letterSizeDefault) / defaultSize;

    return Container(
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(_borderRadius),
      ),
      child: Align(
        alignment: Alignment.center,
        child: Text("${ firstLetter }",
          style: TextStyle(
            color: Colors.black,
            fontWeight: FontWeight.bold,
            fontSize: fontSize,
          ),
        ),
      ),
    );
  }

}
