
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:bridge/main.dart';
import 'package:bridge/services/utilities/util_service.dart';
import 'package:bridge/widgets/camera/camera.dart';
import 'package:bridge/shared/file_uploader/file_uploader.dart';
import 'package:bridge/Translate.dart';

class ImageChoosing extends StatelessWidget {

  late Translate _intl;

  @override
  Widget build(BuildContext context) {

    _intl = Translate.of(context);

    return AnnotatedRegion<SystemUiOverlayStyle>(
      value: const SystemUiOverlayStyle(
        statusBarColor: Colors.transparent,
        statusBarBrightness: Brightness.light,
        statusBarIconBrightness: Brightness.light,
        systemNavigationBarColor: Colors.black,
        systemNavigationBarIconBrightness: Brightness.light,
        systemNavigationBarDividerColor: Colors.black,
      ),
      child: Container(
        height: 175,
        padding: EdgeInsets.all(10),
        decoration: BoxDecoration(
          color: Colors.black87,
          borderRadius: BorderRadius.only(topLeft: Radius.circular(8), topRight: Radius.circular(8)),
        ),
        child: Column(
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                GestureDetector(
                  onTap: () async {
                    await mainStore.imageChossing.clear();
                    Navigator.pop(context);
                  },
                  child: Container(
                    child: Image.asset('assets/icons/none.png', scale: 1.80, color: Colors.white70),
                  ),
                ),
                Text('${ _intl.translate('COMPONENT.IMAGE_CHOOSING.TITLE') }', style: TextStyle(fontSize: 15, fontWeight: FontWeight.normal, color: Colors.white)),
                Container(),
              ],
            ),
            const SizedBox(height: 10),
            Container(
              height: 0.18,
              decoration: BoxDecoration(
                color: Colors.grey,
                borderRadius: BorderRadius.circular(18),
              ),
            ),
            const SizedBox(height: 20),
            Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    GestureDetector(
                      onTap: () => Navigator.push(context, UtilService.createRoute(Camera(cameras: mainStore.cameras!))),
                      child: Container(
                        padding: const EdgeInsets.only(top: 15, bottom: 15, right: 12, left: 12),
                        decoration: BoxDecoration(
                          border: Border.all(color: Colors.grey, width: 0.5),
                          borderRadius: BorderRadius.circular(8),
                        ),
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Image.asset('assets/icons/icon_take_a_photo.png', color: Colors.grey),
                            const SizedBox(height: 8),
                            Text('${ _intl.translate('COMPONENT.IMAGE_CHOOSING.BUTTON.CAMERA') }', style: TextStyle(fontSize: 16, color: Colors.grey)),
                          ],
                        ),
                      ),
                    ),
                    const SizedBox(width: 100),
                    GestureDetector(
                      onTap: () => Navigator.push(context, UtilService.createRoute(FileUploader())),
                      child: Container(
                        padding: EdgeInsets.only(top: 15, bottom: 15, right: 12, left: 12),
                        decoration: BoxDecoration(
                          border: Border.all(color: Colors.grey, width: 0.5),
                          borderRadius: BorderRadius.circular(8),
                        ),
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Image.asset('assets/icons/icon_image.png', color: Colors.grey),
                            const SizedBox(height: 8),
                            Text('${ _intl.translate('COMPONENT.IMAGE_CHOOSING.BUTTON.UPLOAD') }', style: TextStyle(fontSize: 15, color: Colors.grey)),
                          ],
                        ),
                      ),
                    ),
                  ],
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}

