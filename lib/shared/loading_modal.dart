
import 'package:flutter/material.dart';
import 'package:bridge/Translate.dart';
import 'package:bridge/shared/loading.dart';

class LoadingModal extends StatefulWidget {

  final String message;
  double? height;

  LoadingModal({Key? key, required this.message, this.height }) : super(key: key);

  @override
  _LoadingModalState createState() => _LoadingModalState();

}

class _LoadingModalState extends State<LoadingModal> {

  late Translate _intl;

  @override
  void initState() {
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {

    _intl = Translate.of(context);

    return Container(
      height: widget.height ?? 100,
      color: Colors.black,
      child: Column(
        children: [
          Loading(),
          Text('${ widget.message }', //?? '${ _intl.translate('COMPONENT.MODAL.LOADING.DEFAULT') }' }',
            style: TextStyle(color: Colors.white, fontSize: 12)),
        ],
      ),
    );
  }
}
