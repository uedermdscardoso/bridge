
import 'package:flutter/material.dart';
import 'package:skeleton_text/skeleton_text.dart';

class SkeletonLoading extends StatelessWidget {

  final double width;
  final double height;

  const SkeletonLoading({Key? key, required this.width, required this.height }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SkeletonAnimation(
      gradientColor: Colors.grey,
      shimmerColor: Colors.grey[300]!,
      borderRadius: BorderRadius.circular(20),
      shimmerDuration: 1000,
      child: Container(
        width: width,
        height: height,
        decoration: BoxDecoration(
          color: Colors.grey,
          borderRadius: BorderRadius.circular(8),
        ),
      ),
    );
  }
}
