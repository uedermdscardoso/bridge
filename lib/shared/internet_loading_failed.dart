
import 'package:flutter/material.dart';
import 'package:bridge/Translate.dart';

class InternetLoadingFailed extends StatelessWidget {

  late Translate _intl;

  @override
  Widget build(BuildContext context) {

    _intl = Translate.of(context);

    return Center(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Image.asset('assets/icons/internet_loading_failed.png', color: Colors.grey[400], scale: 1.0),
          const SizedBox(height: 25),
          Text('${ _intl.translate('CONNECTION.INTERNET.FAILED') }', style: TextStyle(color: Colors.grey[500], decoration: TextDecoration.none, fontSize: 15, fontWeight: FontWeight.normal, fontFamily: 'Arial')),
          const SizedBox(height: 9),
          Text('${ _intl.translate('CONNECTION.INTERNET.CHECK') }', style: TextStyle(color: Colors.grey[500], decoration: TextDecoration.none, fontSize: 15, fontWeight: FontWeight.normal, fontFamily: 'Arial')),
        ],
      ),
    );
  }
}
