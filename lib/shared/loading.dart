
import 'package:flutter/material.dart';
import 'package:bridge/domain/model/utilities/loading_icon_type.dart';

class Loading extends StatelessWidget {

  LoadingIconType type;
  String? message;

  Loading({Key? key, this.type : LoadingIconType.THREE_BALLS, this.message }) : super(key: key);

  @override
  Widget build(BuildContext context) {

    return Visibility(
      visible: type == LoadingIconType.CIRCLE,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Expanded(
            child: CircularProgressIndicator(
              color: Colors.grey[500],
            ),
          ),
          const SizedBox(height: 15),
          Text('${ message }',
              maxLines: 2,
              textAlign: TextAlign.center,
              overflow: TextOverflow.ellipsis,
              style: TextStyle(height: 1.25, color: Colors.grey[200], fontSize: 12)),
        ],
      ),
      replacement: Padding(
        padding: const EdgeInsets.only(bottom: 8),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Expanded(
              child: Container(
                color: Colors.transparent,
                child: Image.asset('assets/icons/loading.gif', scale: 2.5),
              ),
            ), //4
            Visibility(
              visible: message != null,
              child: Padding(
                padding: const EdgeInsets.only(top: 8),
                child: Text('${ message }', textAlign: TextAlign.center,
                  style: TextStyle(height: 1.4, color: Colors.grey, decoration: TextDecoration.none, fontSize: 14, fontWeight: FontWeight.normal, fontFamily: 'Arial')),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
