
import 'dart:io';
import 'dart:typed_data';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:bridge/Translate.dart';
import 'package:bridge/domain/model/utilities/loading_icon_type.dart';
import 'package:bridge/main.dart';
import 'package:bridge/services/utilities/util_service.dart';
import 'package:bridge/shared/file_uploader/preview_screen.dart';
import 'package:bridge/shared/loading.dart';
import 'package:bridge/widgets/header.dart';

class FileUploader extends StatefulWidget {

  const FileUploader({Key? key}) : super(key: key);

  @override
  State<StatefulWidget> createState() => _FileUploaderState();

}

class _FileUploaderState extends State<FileUploader> with AutomaticKeepAliveClientMixin {

  List<Uint8List> _images = List<Uint8List>.empty();
  int _position = -1;
  List<String> _submenus = List<String>.empty();
  String _selectedItem = 'Camera';
  bool _isLoading = false;

  late Translate _intl;

  @override
  void initState() {
    _loadContent();
    super.initState();
  }

  @override
  void dispose() {
    mainStore.fileUploader.resetImagesList();
    super.dispose();
  }

  _loadContent() {
    mainStore.fileUploader.addFolders();
    mainStore.fileUploader.addFilesByPage(selected: _selectedItem, total: 30);
  }

  _loadData() async {
    await Future.delayed(Duration(seconds: 2));

    setState(() {
      mainStore.fileUploader.addFilesByPage(selected: _selectedItem, total: 30);
      _isLoading = false;
    });
  }

  @override
  Widget build(BuildContext context) {

    _intl = Translate.of(context);

    return AnnotatedRegion<SystemUiOverlayStyle>(
      value: const SystemUiOverlayStyle(
        statusBarColor: Colors.transparent,
        statusBarBrightness: Brightness.light,
        statusBarIconBrightness: Brightness.light,
        systemNavigationBarColor: Colors.black,
        systemNavigationBarIconBrightness: Brightness.light,
        systemNavigationBarDividerColor: Colors.black,
      ),
      child: WillPopScope(
        onWillPop: () async {
          Navigator.pop(context);
          return true;
        },
        child: Material(
          color: Colors.black,
          child: Column(
            children: [
              Header(
                color: Colors.black,
                left: Row(
                  children: [
                    GestureDetector(
                        onTap:() {
                          Navigator.pop(context);
                        },
                        child: Icon(Icons.close, color: Colors.grey, size: 35)
                    ),
                    const SizedBox(width: 8), //8
                    Text('${ _intl.translate('SCREEN.PHOTO_GRID.TITLE') }',
                        style: TextStyle(color: Colors.grey, decoration: TextDecoration.none, fontSize: 22, fontWeight: FontWeight.bold, fontFamily: 'Arial')),
                  ],
                ),
                hasBorder: true,
              ),
              Expanded( //Body
                child: Observer(
                  builder: (_) {

                    return StreamBuilder(
                      stream: mainStore.fileUploader.images,
                      builder: (context, snapshot){

                        if(snapshot.hasData){

                          _images = snapshot.data as List<Uint8List>;

                          return Column(
                            mainAxisAlignment: MainAxisAlignment.start,
                            children: [
                              Expanded(
                                child: NotificationListener<OverscrollIndicatorNotification>(
                                  onNotification: (overscroll) {
                                    overscroll.disallowIndicator();
                                    return false;
                                  },
                                  child: NotificationListener<ScrollNotification>(
                                    onNotification: (ScrollNotification scrollInfo) {
                                      if (!_isLoading && scrollInfo.metrics.pixels == scrollInfo.metrics.maxScrollExtent) {
                                        _loadData();
                                        setState(() {
                                          _isLoading = true;
                                        });
                                      }

                                      return false;
                                    },
                                    child: GridView.count(
                                      padding: EdgeInsets.all(4),
                                      crossAxisSpacing: 2,
                                      mainAxisSpacing: 2,
                                      crossAxisCount: 3,
                                      children: List<Widget>.generate(_images.length, (index) {

                                        final Uint8List image = _images.elementAt(index);

                                        return GestureDetector(
                                          onTap: () async {
                                            final int currentPosition = await mainStore.fileUploader.currentPosition;

                                            setState(() {
                                              if(currentPosition != index)
                                                mainStore.fileUploader.addCurrentPosition(currentPosition: index);
                                              else
                                                mainStore.fileUploader.addCurrentPosition(currentPosition: -1);
                                            });
                                          },
                                          child: Padding(
                                            padding: const EdgeInsets.all(2),
                                            child: Stack(
                                              children: [
                                                Stack(
                                                  fit: StackFit.expand,
                                                  children: [
                                                    ClipRRect(
                                                      borderRadius: BorderRadius.circular(12),
                                                      child: Image.memory(image,
                                                        fit: BoxFit.cover,
                                                        filterQuality: FilterQuality.low,
                                                      ),
                                                    ),
                                                  ],
                                                ),
                                                FutureBuilder(
                                                  future: mainStore.fileUploader.currentPosition,
                                                  builder: (context, snapshot) {

                                                    if(snapshot.hasData)
                                                      _position = snapshot.data as int;

                                                    return Align(
                                                        alignment: Alignment.topRight,
                                                        child: Padding(
                                                          padding: const EdgeInsets.only(top: 8, right: 8),
                                                          child: Container(
                                                            width: 25,
                                                            height: 25,
                                                            decoration: BoxDecoration(border: Border.all(color: index != _position ? Colors.grey : Colors.white, width: 2.5), borderRadius: BorderRadius.circular(100)),
                                                            child: Visibility(
                                                              visible: index == _position,
                                                              child: Icon(Icons.check, color: Colors.white, size: 15),
                                                            ),
                                                          ),
                                                        )
                                                    );
                                                  },
                                                ),
                                              ],
                                            ),
                                          ),
                                        );
                                      }),
                                    ),
                                  ),
                                ),
                              ),
                              Container(
                                color: Colors.black,
                                height: _isLoading ? 100 : 0,
                                child: Loading(type: LoadingIconType.THREE_BALLS, message: ""),
                              ),
                            ],
                          );
                        }
                        return Container(); //Image not found
                      },
                    );
                  },
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(left: 12, right: 24),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    FutureBuilder(
                      future: mainStore.fileUploader.submenus,
                      builder: (context, snapshot) {
                        if(snapshot.hasData) {

                          _submenus = snapshot.data as List<String>;

                          return DropdownButton<String>(
                            value: _submenus.contains(_selectedItem) ? _selectedItem : null,
                            hint: Text(_intl.translate('SCREEN.PHOTO_GRID.DROPDOWN.CHOOSE_FOLDER'),
                                style: const TextStyle(color: Colors.grey, fontSize: 16)),
                            dropdownColor: Colors.black,
                            icon: const Icon(Icons.arrow_drop_down, color: Colors.grey),
                            iconSize: 24,
                            elevation: 1,
                            underline: Container(height: 0),
                            onChanged: (item) async {
                              setState(() => _selectedItem = item! );

                              await mainStore.fileUploader.resetImagesList();
                              await mainStore.fileUploader.addFilesByPage(selected: _selectedItem, total: 30);
                            },
                            items: _submenus.map<DropdownMenuItem<String>>((String value) {
                              String text = '${ value.substring(0,1).toUpperCase() }${ value.substring(1,value.length) }';

                              return DropdownMenuItem<String>(
                                value: value,
                                child: SizedBox(
                                  width: 150,
                                  child: Text(text, style: TextStyle(color: Colors.grey, fontSize: 18, fontWeight: FontWeight.normal)),
                                ),
                              );
                            }).toList(),
                          );
                        }

                        return Container();
                      },
                    ),
                    Container(
                      color: Colors.black,
                      height: 75,
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.end,
                        children: [
                          GestureDetector(
                            onTap: () async {
                              if(_position >= 0) {
                                final Uint8List bytes = await _images.elementAt(_position);
                                final File image = File.fromRawPath(bytes);

                                Navigator.push(context, UtilService.createRoute(PreviewScreen(preview: image)));
                              }
                            },
                            child: Text('${ _intl.translate('SCREEN.PHOTO_GRID.BUTTON.CHOOSE') }',
                              style: TextStyle(color: _position < 0 ? Colors.grey : Colors.grey, decoration: TextDecoration.none, fontSize: 18, fontWeight: _position == 0 ? FontWeight.normal : FontWeight.bold, fontFamily: 'Arial'))
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  @override
  bool get wantKeepAlive => true;

}
