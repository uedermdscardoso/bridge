
import 'package:bridge/domain/model/media/media_type.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:bridge/Translate.dart';
import 'package:bridge/domain/model/media/media.dart';
import 'package:bridge/main.dart';
import 'package:bridge/shared/custom_field.dart';
import 'package:bridge/widgets/header.dart';
import 'package:bridge/shared/media_upload/media_library.dart';

class MediaUpload extends StatefulWidget {

  final MediaType type;

  const MediaUpload({ Key? key, required this.type }) : super(key: key);

  @override
  _MediaUploadState createState() => _MediaUploadState();
}

class _MediaUploadState extends State<MediaUpload> {

  final _searchEditingController = TextEditingController();

  Set<Media> _audios = Set<Media>.of({});
  Set<Media> _soundSearch = Set<Media>.of({});

  late Translate _intl;

  @override
  void initState() {
    mainStore.mediaUpload.addLocalMediaLibrary();
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {

    _intl = Translate.of(context);

    return AnnotatedRegion<SystemUiOverlayStyle>(
      value: const SystemUiOverlayStyle(
        statusBarColor: Colors.transparent,
        statusBarBrightness: Brightness.dark,
        statusBarIconBrightness: Brightness.dark,
        systemNavigationBarIconBrightness: Brightness.dark,
        systemNavigationBarColor: Colors.white,
        systemNavigationBarDividerColor: Colors.white,
      ),
      child: Material(
        child: Container(
          color: Colors.white,
          child: Column(
            children: [
              Header(
                color: Colors.white,
                center: Expanded(
                  child: FutureBuilder(
                    future: mainStore.studio.localSoundLibrary,
                    builder: (context, snapshot) {
                      if(snapshot.hasData) {

                        _audios = snapshot.data as Set<Media>;

                      }

                      return CustomField(
                        controller: _searchEditingController,
                        autofocus: false,
                        //prefixIcon: Icon(Icons.search, color: Colors.grey, size: 25),
                        text: '${ _intl.translate('SCREEN.MEDIA.BUTTON.SEARCH') }',
                        maxLines: 1,
                        backgroundColor: Colors.white,
                        textColor: Colors.grey,
                        borderColor: Colors.white,
                        onChanged: (value){
                          _soundSearch.clear();

                          if(_audios != null && _audios.isNotEmpty) {
                            _audios.forEach((elem) {
                              if(elem.title.startsWith(value)) {
                                setState(() => _soundSearch.add(elem) );
                              }
                            });
                          }
                        },
                      );
                    },
                  ),
                ),
                right: GestureDetector(
                  onTap: () => Navigator.pop(context),
                  child: const Icon(Icons.close, size: 25, color: Colors.grey),
                ),
                hasBorder: true,
              ),
              const SizedBox(height: 8),
              Expanded(
                child: Padding(
                  padding: const EdgeInsets.all(8),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      Expanded(
                        child: MediaLibrary(type: widget.type, search: _soundSearch),
                      ),
                      const SizedBox(height: 12),
                      Container(),
                    ],
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
