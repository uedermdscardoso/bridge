
import 'package:flutter/material.dart';
import 'package:bridge/Translate.dart';

class NoResultFound extends StatelessWidget {

  IconData? icon;
  Image? image;
  Color? color;

  late Translate _intl;

  NoResultFound({Key? key, this.icon, this.image, this.color }) : super(key: key);

  @override
  Widget build(BuildContext context) {

    _intl = Translate.of(context);

    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        image ?? Icon(this.icon ?? Icons.data_usage, color: this.color ?? Colors.grey[400], size: 35),
        const SizedBox(height: 15),
        Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Text('${ _intl.translate('COMPONENT.WARNING.NO_RESULT_FOUND') }',
                style: TextStyle(color: this.color ?? Colors.grey[500], fontSize: 12, fontWeight: FontWeight.normal)),
          ],
        ),
      ],
    );
  }
}
