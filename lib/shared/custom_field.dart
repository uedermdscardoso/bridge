
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

class CustomField extends StatelessWidget {

  TextEditingController? controller;
  bool? autofocus;
  Color? backgroundColor;
  Color? textColor;
  Color? borderColor;
  String? text;
  TextAlign? textAlign;
  bool? obscureText;
  ValueChanged? onChanged;
  Widget? prefixIcon;
  Widget? suffixIcon;
  Widget? suffix;
  TextAlignVertical? textAlignVertical;
  int? maxLines;
  bool? readOnly;
  TextStyle? style;
  List<TextInputFormatter>? inputFormatters;
  TextInputType? keyboardType;
  EdgeInsetsGeometry? contentPadding;
  int? maxLength;
  InputCounterWidgetBuilder? buildCounter;

  CustomField({
    Key? key,
    this.controller,
    this.maxLines,
    this.textAlignVertical,
    this.autofocus,
    this.backgroundColor,
    this.textColor,
    this.borderColor,
    this.text,
    this.textAlign,
    this.obscureText,
    this.onChanged,
    this.prefixIcon,
    this.suffixIcon,
    this.suffix,
    this.readOnly,
    this.style,
    this.inputFormatters,
    this.keyboardType,
    this.contentPadding,
    this.maxLength,
    this.buildCounter
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Material(
      color: Colors.transparent,
      child: TextField(
        maxLength: this.maxLength,
        buildCounter: this.buildCounter,
        keyboardType: this.keyboardType ?? TextInputType.text,
        inputFormatters: this.inputFormatters,
        autofocus: this.autofocus ?? false,
        readOnly: this.readOnly ?? false,
        obscureText: this.obscureText ?? false,
        controller: this.controller,
        maxLines: this.maxLines ?? 1,
        textAlign: this.textAlign ?? TextAlign.start,
        style: this.style ?? TextStyle(color: this.textColor ?? Colors.grey),
        onChanged: this.onChanged,
        decoration: InputDecoration(
          filled: true,
          fillColor: this.backgroundColor ?? Colors.grey[200]!,
          suffixIcon: this.suffixIcon,
          suffix: this.suffix,
          prefixIcon: this.prefixIcon,
          disabledBorder: OutlineInputBorder(
            borderSide: BorderSide(color: this.borderColor ?? Colors.grey[200]!),
            borderRadius: BorderRadius.circular(8),
          ),
          enabledBorder: OutlineInputBorder(
            borderSide: BorderSide(color: this.borderColor ?? Colors.grey[200]!),
            borderRadius: BorderRadius.circular(8),
          ),
          /*border: OutlineInputBorder(
            borderSide: BorderSide(color: borderColor ?? Colors.transparent),
            borderRadius: BorderRadius.circular(12),
          ),*/
          focusedBorder: OutlineInputBorder(
            borderSide: BorderSide(color: this.borderColor ?? Colors.transparent),
            borderRadius: BorderRadius.circular(8),
          ),
          contentPadding: contentPadding ?? EdgeInsets.only(top: 8, bottom: this.prefixIcon != null ? 8 : 12, left: 8, right: 8),
          hintText: this.text,
          hintStyle: TextStyle(color: this.textColor ?? Colors.grey),
        ),
        textAlignVertical: this.textAlignVertical ?? TextAlignVertical.center,
      ),
    );
  }
}
