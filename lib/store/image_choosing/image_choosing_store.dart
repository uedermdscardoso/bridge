
import 'package:mobx/mobx.dart';
import 'dart:io';
import 'dart:async';

import 'package:bridge/domain/model/utilities/file_uploader_type.dart';
import 'package:bridge/shared/file_uploader/file_uploader.dart';

part 'image_choosing_store.g.dart';

class ImageChoosingStore = _ImageChoosingStore with _$ImageChoosingStore;

abstract class _ImageChoosingStore with Store {

  @observable
  Future<File>? tempImage;

  @observable
  Future<FileUploaderType> type = Future.value(FileUploaderType.NONE);

  @action
  addTempImage({ required File image }) => this.tempImage = Future.value(image);

  @action
  addFileUploaderType({ required FileUploaderType type }) => this.type = Future.value(type);

  @action
  clear() {
    this.tempImage = Future.value(null);
    this.type = Future.value(FileUploaderType.NONE);
  }

}