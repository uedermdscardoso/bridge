import 'dart:typed_data';

import 'package:mobx/mobx.dart';
import 'package:photo_manager/photo_manager.dart';
import 'package:rxdart/rxdart.dart';
import 'dart:async';

part 'file_uploader_store.g.dart';

class FileUploaderStore = _FileUploaderStore with _$FileUploaderStore;

abstract class _FileUploaderStore with Store {

    @observable
    Future<List<AssetPathEntity>> folders = Future.value([]);

    @observable
    Future<int> currentPosition = Future.value(-1);

    @observable
    BehaviorSubject<List<Uint8List>> images = BehaviorSubject<List<Uint8List>>();

    @observable
    Future<List<String>> submenus = Future.value([]);

    @action
    addFolders() async {
        var result = await PhotoManager.requestPermissionExtend();
        if(result.hasAccess) {
            final List<AssetPathEntity> folders = await PhotoManager.getAssetPathList(type: RequestType.image, hasAll: true);
            folders.sort((a,b) => a.name.compareTo(b.name));
            folders.sort((a,b) => a.name.compareTo(b.name));

            List<String> submenus = [];
            folders.forEach((item) => submenus.add(item.name));

            this.submenus = Future.value(submenus);
            this.folders = Future.value(folders);
        }
    }

    @action
    addCurrentPosition({ required int currentPosition }) {
        this.currentPosition = Future.value(currentPosition);
    }

    @action
    addFilesByPage({ required String selected, required int total }) async {
        final List<AssetPathEntity> folders = await this.folders;

        if(folders != null && folders.isNotEmpty) {
            final List<AssetPathEntity> items = folders.where((item) => item.name.compareTo(selected) == 0).toList();

            if(items.isNotEmpty) {
                final List<AssetEntity> images = await items.elementAt(0).getAssetListPaged(page: 1, size: 1);

                if(this.images.hasValue) {
                    final List<Uint8List> lastImages = this.images.stream.value;
                    final List<AssetEntity> cuted = images.getRange(lastImages.length, lastImages.length+total).toList();

                    List<Uint8List> newImages = lastImages;
                    cuted.forEach((e) async => newImages.add((await e.thumbnailData)!));

                    this.images.add(newImages);
                } else {
                    final List<AssetEntity> cuted = images.getRange(0, total).toList();

                    List<Uint8List> newImages = [];
                    cuted.forEach((e) async => newImages.add((await e.thumbnailData)!));

                    this.images.add(newImages);
                }
            }
        }
    }

    @action
    resetImagesList() {
        if(this.images.hasValue) {
          this.images.value.clear();
        }
    }

}