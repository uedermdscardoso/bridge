
import 'dart:io';

import 'package:bridge/domain/model/media/media_type.dart';
import 'package:just_audio/just_audio.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:bridge/Translate.dart';
import 'package:bridge/domain/model/media/cutted_media.dart';
import 'package:bridge/domain/model/media/cutted_media_dto.dart';
import 'package:bridge/domain/model/media/media.dart';
import 'package:bridge/domain/model/utilities/loading_icon_type.dart';
import 'package:bridge/main.dart';
import 'package:bridge/services/media/media_service.dart';
import 'package:bridge/services/utilities/util_service.dart';
import 'package:bridge/widgets/header.dart';
import 'package:bridge/widgets/studio/recording_studio.dart';

class AudioCut extends StatefulWidget {

  final Media media;

  const AudioCut({ Key? key, required this.media }) : super(key: key);

  @override
  _AudioCutState createState() => _AudioCutState();
}

class _AudioCutState extends State<AudioCut> with TickerProviderStateMixin {

  final _IMAGE_ASSET = 'assets/images';

  final ScrollController _controller = ScrollController();
  final ScrollController _scrollCtrl = new ScrollController(initialScrollOffset: 0);
  final MediaService _mediaService = MediaService();

  static const double _VELOCITY = 1.5;

  final AudioPlayer _player = AudioPlayer();

  late AnimationController _animateCtrl;
  double offset = 0.0;
  double distance = 0.0;
  double _initialSecond = 0.0;
  double _initial = 0.0;
  double _dx = 0.0;
  double _total = 0.0;
  double leftPos = 0.0;
  double rightPos = 0.0;
  double _widthCurrent = 0.0;
  double _trimSeconds = 0.0;

  int _totalDuration = 0;
  bool _showIndicator = false;
  bool _isLeft = false;

  late Translate _intl;

  @override
  void initState() {
    _player.setUrl(widget.media.path);
    _player.play();
    _totalDuration = _getTotal(duration: widget.media.duration);
    _totalDuration = _totalDuration < 5 ? 5 : _totalDuration;
    _animateText();
    super.initState();
  }

  @override
  void dispose() {
    _player.stop();
    _controller.dispose();
    _scrollCtrl.dispose();
    _animateCtrl.dispose();
    super.dispose();
  }

  _animateText() {
    offset = (_totalDuration * 70).toDouble();
    _animateCtrl = AnimationController(vsync: this, duration: Duration(seconds: 1))..addListener(() {
      if(_animateCtrl.isCompleted)
        _animateCtrl.repeat();
      offset -= 4.0; //6.0

      final double durationCurrent = ((_widthCurrent - 42) / 70); //5

      if((offset / 70) <= (_totalDuration - durationCurrent)) { //5 is default
        offset = (_totalDuration * 70).toDouble();
        _changeCurrentPosition();
      }

      if(_scrollCtrl.positions.isNotEmpty)
        setState(() => _scrollCtrl.jumpTo(offset));
    });
    _animateCtrl.forward();
    _animateCtrl.repeat();
  }

  _changeStatusAudio() async {
    final int currentDuration = await _player.position.inMilliseconds; //getCurrentPosition();
    if(!_showIndicator) {
      //print('currentDuration: ${ currentDuration / 1000 }, _trimSeconds: ${ _trimSeconds }');
      if ((currentDuration / 1000) > (currentDuration + _trimSeconds)) {
        _player.stop();
        _player.seek(Duration(milliseconds: (_initialSecond * 1000).toInt()));
      } else {
        _player.play(); //resume();
      }
    }
  }

  @override
  Widget build(BuildContext context) {

    _intl = Translate.of(context);

    _total = MediaQuery.of(context).size.width;
    _widthCurrent = _total - (leftPos + rightPos);

    _changeStatusAudio();

    return Material(
      color: Colors.black,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        children: [
          Header(
            showLeft: true,
            color: Colors.transparent,
            right: GestureDetector(
              onTap: () => _stitchAudio(context: context),
              child: Container(
                padding: EdgeInsets.only(top: 8, bottom: 8, right: 18, left: 18),
                decoration: BoxDecoration(
                  color: Colors.amber[600],
                  borderRadius: BorderRadius.circular(12),
                ),
                child: Text('Next', style: TextStyle(color: Colors.black)),
              ),
            ),
            hasBorder: false,
          ),
          Expanded(
            child: Stack(
              fit: StackFit.expand,
              children: [
                ClipRRect(
                  borderRadius: BorderRadius.circular(18),
                  child: CachedNetworkImage(
                    imageUrl: widget.media.image,
                    width: 180, //175
                    height: 180, //175
                    fit: BoxFit.cover,
                    placeholder: (context, url) => Image.asset('assets/images/anonymous.jpg', fit: BoxFit.cover),
                    errorWidget: (context, url, error) => Image.asset('assets/images/anonymous.jpg', fit: BoxFit.cover),
                  ),
                ),
                Container(
                  decoration: BoxDecoration(
                    gradient: LinearGradient(begin: Alignment.topRight, stops: [
                      0.1,
                      0.9
                    ], colors: [
                      Colors.black.withOpacity(.90),
                      Colors.black.withOpacity(.20),
                    ]),
                  ),
                  child: Container(
                    decoration: BoxDecoration(
                      gradient: LinearGradient(begin: Alignment.bottomRight, stops: [
                        0.1,
                        0.9
                      ], colors: [
                        Colors.black.withOpacity(.90),
                        Colors.black.withOpacity(.20),
                      ]),
                    ),
                  ),
                ),
                Column(
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: [
                    Expanded(
                      child: Padding(
                        padding: const EdgeInsets.only(bottom: 50, left: 80, right: 80),
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.end,
                          children: [
                            Text('Cut an audio to add your comment and share the world.',
                              overflow: TextOverflow.ellipsis,
                              maxLines: 4,
                              textAlign: TextAlign.center,
                              style: TextStyle(height: 1.5, color: Colors.grey, fontSize: 15)),
                          ],
                        ),
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(right: 8, left: 8, bottom: 18),
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Container(
                            height: 75,
                            child: Stack(
                              children: [
                                ListView.builder(
                                  padding: EdgeInsets.only(left: 12, right: 12),
                                  controller: _controller,
                                  itemCount: _totalDuration,
                                  scrollDirection: Axis.horizontal,
                                  itemBuilder: (context, index) {

                                    return Row(
                                      children: [
                                        CachedNetworkImage(
                                          imageUrl: widget.media.image,
                                          width: 35,
                                          height: 75,
                                          fit: BoxFit.cover,
                                          placeholder: (context, url) => Image.asset('assets/images/anonymous.jpg', fit: BoxFit.cover),
                                          errorWidget: (context, url, error) => Image.asset('assets/images/anonymous.jpg', fit: BoxFit.cover),
                                        ),
                                        CachedNetworkImage(
                                          imageUrl: widget.media.image,
                                          width: 35,
                                          height: 75,
                                          fit: BoxFit.cover,
                                          placeholder: (context, url) => Image.asset('assets/images/anonymous.jpg', fit: BoxFit.cover),
                                          errorWidget: (context, url, error) => Image.asset('assets/images/anonymous.jpg', fit: BoxFit.cover),
                                        ),
                                      ],
                                    );
                                  },
                                ),
                                Row(
                                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                  children: [
                                    Listener(
                                      onPointerMove: (PointerEvent event) {
                                        setState(() {
                                          _initial = distance.isNegative ?
                                            _controller.offset - (event.position.direction * _VELOCITY) :
                                            _controller.offset + (event.position.direction * _VELOCITY);
                                        });

                                        _controller.jumpTo(_initial);
                                      },
                                      child: GestureDetector(
                                        onPanStart: (DragStartDetails details) {
                                          _dx = details.globalPosition.dx;
                                        },
                                        onPanUpdate: (DragUpdateDetails details) {
                                          distance = details.globalPosition.dx - _dx;
                                        },
                                        onPanEnd: (DragEndDetails details) {
                                          _dx = 0.0;
                                        },
                                        child: Container(
                                          width: !leftPos.isNegative ? leftPos : leftPos * -1,
                                          color: Colors.black.withOpacity(0.6),
                                        ),
                                      ),
                                    ),
                                    Expanded(
                                      child: Container(
                                        height: 75,
                                        decoration: BoxDecoration(
                                          color: Colors.transparent,
                                          border: Border.all(color: Colors.amber[700]!, width: 2.5),
                                        ),
                                        child: Row(
                                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                          children: [
                                            Listener(
                                              onPointerMove: (PointerEvent event) {
                                                final double leftPosLocal = event.position.dx;
                                                final double width = _total - (leftPosLocal + rightPos);

                                                if(width > 110)
                                                  setState(() => leftPos = leftPosLocal);
                                              },
                                              onPointerUp: (PointerUpEvent event) {
                                                setState(() => offset = (_totalDuration * 70).toDouble());

                                                _player.play(); //resume();

                                                if(_scrollCtrl.positions.isNotEmpty) {
                                                  setState(() {
                                                    _showIndicator = false;
                                                    _isLeft = true;
                                                  });

                                                  _changeCurrentPosition();

                                                  _animateCtrl.repeat();
                                                }
                                              },
                                              onPointerDown: (PointerDownEvent event) {
                                                _player.stop();
                                                _scrollCtrl.jumpTo(0.0);
                                                _animateCtrl.stop();

                                                if(_scrollCtrl.positions.isNotEmpty) {
                                                  setState(() {
                                                    _showIndicator = true;
                                                    _isLeft = true;
                                                  });
                                                }
                                              },
                                              child: Container(
                                                width: 12,
                                                height: 75,
                                                decoration: BoxDecoration(
                                                  //borderRadius: BorderRadius.only(topLeft: Radius.circular(8), bottomLeft: Radius.circular(8)),
                                                  border: Border.all(color: Colors.amber[700]!, width: 0.5),
                                                  color: Colors.amber[700],
                                                ),
                                                child: Icon(Icons.arrow_forward_ios_outlined, color: Colors.black, size: 12),
                                              ),
                                            ),
                                            Expanded(
                                              child: Stack(
                                                children: [
                                                  Listener(
                                                    onPointerMove: (PointerEvent event) {
                                                      setState(() {
                                                        _initial = distance.isNegative ?
                                                        _controller.offset - (event.position.direction * _VELOCITY) :
                                                        _controller.offset + (event.position.direction * _VELOCITY);
                                                      });

                                                      _controller.jumpTo(_initial);
                                                    },
                                                    child: GestureDetector(
                                                      onPanStart: (DragStartDetails details) {
                                                        _dx = details.globalPosition.dx;
                                                      },
                                                      onPanUpdate: (DragUpdateDetails details) {
                                                        distance = details.globalPosition.dx - _dx;
                                                      },
                                                      onPanEnd: (DragEndDetails details) {
                                                        _dx = 0.0;
                                                      },
                                                      child: Container(
                                                        color: Colors.transparent,
                                                      ),
                                                    ),
                                                  ),
                                                  ListView.builder(
                                                    scrollDirection: Axis.horizontal,
                                                    controller: _scrollCtrl,
                                                    itemBuilder: (context, index) {

                                                      return Padding(
                                                        padding: EdgeInsets.only(left: (_totalDuration * 70).toDouble(), right: _widthCurrent / 2),
                                                        child: Container(
                                                          width: 6,
                                                          decoration: BoxDecoration(
                                                            color: Colors.white,
                                                          ),
                                                        ),
                                                      );
                                                    }
                                                  ),
                                                  Visibility(
                                                    visible: _showIndicator,
                                                    child: Align(
                                                      alignment: _isLeft ? Alignment.centerLeft : Alignment.centerRight,
                                                      child: Container(
                                                        width: 6,
                                                        decoration: BoxDecoration(
                                                          color: Colors.white,
                                                        ),
                                                      ),
                                                    ),
                                                  ),
                                                ],
                                              ),
                                            ),
                                            Listener(
                                              onPointerMove: (PointerEvent event) {
                                                final double rightPosLocal = _total - event.position.dx;
                                                final double width = _total - (leftPos + rightPosLocal);

                                                if(width > 110)
                                                  setState(() => rightPos = rightPosLocal);
                                              },
                                              onPointerUp: (PointerUpEvent event) {
                                                setState(() => offset = (_totalDuration * 70).toDouble());

                                                _player.play(); //resume();

                                                if(_scrollCtrl.positions.isNotEmpty) {
                                                  setState(() {
                                                    _showIndicator = false;
                                                    _isLeft = false;
                                                  });

                                                  _changeCurrentPosition();
                                                  _animateCtrl.repeat();
                                                }
                                              },
                                              onPointerDown: (PointerDownEvent event) {
                                                _scrollCtrl.jumpTo(0.0);
                                                _animateCtrl.stop();
                                                _player.stop();

                                                if(_scrollCtrl.positions.isNotEmpty) {
                                                  setState(() {
                                                    _showIndicator = true;
                                                    _isLeft = false;
                                                  });
                                                  _animateCtrl.stop();
                                                }
                                              },
                                              child: Container(
                                                width: 12,
                                                height: 75,
                                                decoration: BoxDecoration(
                                                  //borderRadius: BorderRadius.only(topRight: Radius.circular(8), bottomRight: Radius.circular(8)),
                                                  border: Border.all(color: Colors.amber[700]!, width: 0.5),
                                                  color: Colors.amber[700],
                                                ),
                                                child: Icon(Icons.arrow_back_ios_outlined, color: Colors.black, size: 12),
                                              ),
                                            ),
                                          ],
                                        ),
                                      ),
                                    ),
                                    Listener(
                                      onPointerMove: (PointerEvent event) {
                                        setState(() {
                                          _initial = distance.isNegative ?
                                          _controller.offset - (event.position.direction * _VELOCITY) :
                                          _controller.offset + (event.position.direction * _VELOCITY);
                                        });

                                        _controller.jumpTo(_initial);
                                      },
                                      child: GestureDetector(
                                        onPanStart: (DragStartDetails details) {
                                          _dx = details.globalPosition.dx;
                                        },
                                        onPanUpdate: (DragUpdateDetails details) {
                                          distance = details.globalPosition.dx - _dx;
                                        },
                                        onPanEnd: (DragEndDetails details) {
                                          _dx = 0.0;
                                        },
                                        child: Container(
                                          width: !rightPos.isNegative ? rightPos : rightPos * -1,
                                          color: Colors.black.withOpacity(0.6),
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                              ],
                            ),
                          ),
                          const SizedBox(height: 8),
                          Align(
                            alignment: Alignment.centerLeft,
                            child: Text('${ _getDurationSelected(current: _widthCurrent) } seconds',
                              style: TextStyle(color: Colors.grey[400], fontWeight: FontWeight.bold)),
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }

  _getTotal({ required int duration }) {
    final double seconds = duration / 1000;
    final int unidade = (duration / 1000).toInt();

    if(unidade < seconds) {

      return unidade + 1;
    }

    return unidade;
  }

  String _getDurationSelected({ required double current }) {
    final double percent = ((current - 38) * 100) / (_total - 38);
    final double seconds = (5 * percent) / 100;

    setState(() => _trimSeconds = seconds);

    return _trimSeconds.toStringAsFixed(1);
  }

  void _changeCurrentPosition() async  {
    final double positionCurrent = (_controller.positions.isNotEmpty ? _controller.offset : 0) + leftPos;

    setState(() => _initialSecond = positionCurrent / 70);
    _player.seek(Duration(milliseconds: (_initialSecond * 1000).toInt()));
  }

  _stitchAudio({ required BuildContext context }) async {

    UtilService.onLoading(context: context, type: LoadingIconType.CIRCLE, isDark: true, message: '${ _intl.translate('COMPONENT.LOADING.DEFAULT.MESSAGE') }');

    final File clippedAudio = await _cutAudio(path: widget.media.path, initialSeconds: _initialSecond, trimSeconds: _trimSeconds);

    final int duration =  (_trimSeconds * 1000).toInt();
    final Duration startDuration = Duration(milliseconds: (_initialSecond * 1000).toInt());

    final CuttedMedia cuttedMedia = CuttedMedia(
      id: widget.media.id,
      startDuration: startDuration.inMilliseconds,
      duration: duration,
      author: widget.media.createdBy!.username,
      image: widget.media.image,
    );

    final CuttedMediaDTO cuttedMediaDTO = CuttedMediaDTO.withAudio(cuttedMedia: cuttedMedia, audio: clippedAudio);

    mainStore.studio.addCuttedMediaDTO(cuttedMediaDTO: cuttedMediaDTO);

    Navigator.pop(context);

    Navigator.of(context).push(UtilService.createRoute(const RecordingStudio(
        type: MediaType.STITCHED_AUDIO,
      )),
    );
  }

  Future<File> _cutAudio({ required String path, required double initialSeconds, required trimSeconds }) async {
    final String initialInterval = UtilService.formatDuration(
      duration: Duration(milliseconds: (initialSeconds * 1000).toInt()),
      hasMilliseconds: false,
    );

    final File clippedAudio = await _mediaService.cutAudioInterval(
      path: path,
      initialInterval: initialInterval,
      seconds: trimSeconds,
    );

    return clippedAudio;
  }
}
