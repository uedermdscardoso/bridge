
import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:bridge/widgets/notification/item/like_notification_item.dart';
import 'package:bridge/widgets/notification/item/follow_notification_item.dart';
import 'package:bridge/widgets/notification/item/comment_notification_item.dart';
import 'package:bridge/services/user/user_service.dart';
import 'package:bridge/Translate.dart';
import 'package:bridge/domain/model/user/users.dart';
import 'package:bridge/shared/loading.dart';
import 'package:bridge/shared/no_result_found.dart';
import 'package:bridge/main.dart';

class PersonalNotifications extends StatefulWidget {
  const PersonalNotifications({ Key? key }) : super(key: key);

  @override
  _PersonalNotificationsState createState() => _PersonalNotificationsState();
}

class _PersonalNotificationsState extends State<PersonalNotifications> {

  final ScrollController _scrollController = ScrollController();
  final UserService _userService = UserService();

  late Translate _intl;

  @override
  void initState() {
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {

    _intl = Translate.of(context);

    return Observer(
      builder: (_) {

        return FutureBuilder(
          future: mainStore.notification.notifications,
          builder: (context, snapshot) {

            if(snapshot.hasData) {

              final Query query = snapshot.data as Query;

              return Container();

              /*return RealtimePagination(
                itemsPerPage: 10,
                initialLoading: Loading(),
                emptyDisplay: NoResultFound(icon: Icons.notifications),
                bottomLoader: Loading(),
                itemBuilder: (int index, BuildContext context, DocumentSnapshot document) {

                  return FutureBuilder(
                    future: _userService.getSimpleUserById(user: Users.onlyId(id: document.get('firedBy') ?? '')),
                    builder: (context, snapshot) {

                      if(snapshot.hasData) {

                        final Users user = snapshot.data as Users;

                        return getNotitificationItem(user: user, notification: document);
                      }

                      return Container();
                    },
                  );

                },
                query: query,
                scrollController: _scrollController,
                customPaginatedBuilder: (itemCount, controller, itemBuilder) {

                  return NotificationListener<OverscrollIndicatorNotification>(
                    onNotification: (overscroll) {
                      overscroll.disallowIndicator();

                      return false;
                    },
                    child: ListView.builder(
                      padding: EdgeInsets.only(right: 4, left: 4),
                      itemCount: itemCount,
                      scrollDirection: Axis.vertical,
                      controller: controller,
                      itemBuilder: itemBuilder,
                    ),
                  );
                },
              );*/
            }

            return Container();
          },
        );

        /*return StreamBuilder(
          stream: mainStore.notification.notifications,
          builder: (context, snapshot) {

            if(snapshot.hasData) {

              _notifications = snapshot.data;

              if(_notifications.docs.isNotEmpty) {

                return NotificationListener<OverscrollIndicatorNotification>(
                  onNotification: (overscroll) { overscroll.disallowGlow(); },
                  child: ListView.builder(
                    padding: EdgeInsets.only(right: 4, left: 4),
                    itemCount: _notifications.docs.length ?? 0,
                    itemBuilder: (context, index) {

                      final DocumentSnapshot notification = _notifications.docs.elementAt(index);

                      return FutureBuilder(
                        future: _userService.getSimpleUserById(user: Users(id: notification.get('firedBy'))),
                        builder: (context, snapshot) {

                          if(snapshot.hasData) {

                            final Users user = snapshot.data;

                            return getNotitificationItem(user: user, notification: notification);
                          }

                          return Container();
                        },
                      );
                    },
                  ),
                );
              }
            }

            return NoResultFound(icon: Icons.notifications);
          },
        );*/
      },
    );
  }

  Widget getNotitificationItem({ required DocumentSnapshot notification, required Users user }) {
    if(notification.get('type').compareTo('like_media') == 0){
      return LikeNotificationItem(
        notification: notification,
        firedBy: user,
        type: 'media',
      );
    } else if(notification.get('type').compareTo('like_comment') == 0){
      return LikeNotificationItem(
        notification: notification,
        firedBy: user,
        type: 'comment',
      );
    } else if(notification.get('type').compareTo('follow') == 0){
      return FollowNotificationItem(
          notification: notification,
          firedBy: user
      );
    } else if(notification.get('type').compareTo('comment') == 0){
      return CommentNotificationItem(
        notification: notification,
        firedBy: user,
      );
    }

    return Container();
  }

}
