
import 'dart:io';
import 'dart:typed_data';

import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/services.dart';
import 'package:bridge/domain/model/utilities/loading_icon_type.dart';
import 'package:bridge/shared/loading.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:logger/logger.dart';
import 'package:fluttertoast/fluttertoast.dart';

class UtilService {

  static String captialize({ required String text}) {
    try {
      if (text.isEmpty) {
        throw Exception("Text is empty");
      }

      final String newText = text.replaceRange(0, 1, text[0].toUpperCase());

      return newText;

    } catch(e) {
      throw Exception(e);
    }
  }

  static void onLoading({ required BuildContext context, LoadingIconType? type, bool isDark : false, required String message }) {
    showDialog(
      context: context,
      barrierDismissible: false,
      builder: (BuildContext context) {
        return AnnotatedRegion<SystemUiOverlayStyle>(
          value: const SystemUiOverlayStyle(
            statusBarColor: Colors.transparent,
            statusBarBrightness: Brightness.light,
            statusBarIconBrightness: Brightness.light,
            systemNavigationBarColor: Colors.black45,
            systemNavigationBarIconBrightness: Brightness.light,
            systemNavigationBarDividerColor: Colors.black45,
          ),
          child: WillPopScope(
            onWillPop: () async {
              return false;
            },
            child: Dialog(
              elevation: 0,
              backgroundColor: Colors.transparent,
              child: Container(
                color: Colors.transparent,
                height: 100,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Container(
                      padding: const EdgeInsets.all(8), //25
                      width: 140,
                      height: 100, //250
                      decoration: BoxDecoration(
                        color: !isDark ? Colors.white : Color.fromRGBO(72,72,72, 1.0),
                        borderRadius: BorderRadius.circular(12),
                      ),
                      child: Loading(type: type ?? LoadingIconType.THREE_BALLS, message: message),
                    ),
                  ],
                ),
              ),
            ),
          ),
        );
      },
    );
  }

  static String formatDuration({ required Duration duration, bool hasMilliseconds : false }) {
    String formated = '';

    if(duration != null) {
      String twoDigits(int n) => n.toString().padLeft(2, "0");
      final String twoDigitMinutes = twoDigits(duration.inMinutes);
      final String twoDigitSeconds = twoDigits(duration.inSeconds);

      if(!hasMilliseconds)
        formated = "${twoDigits(duration.inHours)}:$twoDigitMinutes:$twoDigitSeconds";
      else {
        final String twoDigitMilliseconds = twoDigits(duration.inMilliseconds);

        formated = "${twoDigits(duration.inHours)}:$twoDigitMinutes:$twoDigitSeconds:$twoDigitMilliseconds";
      }
    }

    return formated;
  }

  static String formatCustomDuration({ required Duration duration }) {
    String formated = '';

    if(duration != null) {
      String twoDigits(int n) => n.toString().padLeft(2, "0");
      final String twoDigitMinutes = twoDigits(duration.inMinutes.remainder(60));
      final String twoDigitSeconds = twoDigits(duration.inSeconds.remainder(60));

      formated = "$twoDigitMinutes:$twoDigitSeconds";
    }

    return formated;
  }

  static Future<File> writeToTempFile({ required Uint8List data, required String path }) {
    final buffer = data.buffer;
    return new File(path).writeAsBytes(
        buffer.asUint8List(data.offsetInBytes, data.lengthInBytes));
  }

  static Future<Uint8List> getFileFromUrl({ required String url }) async {
    try {
      var request = await HttpClient().getUrl(Uri.parse(url));
      var response = await request.close();
      Uint8List bytes = await consolidateHttpClientResponseBytes(response);

      return bytes;
    } catch(e) {
      throw Exception(e);
    }
  }

  static num getCustomId({ required String first, required String second}){
    try {
      num total = 0;

      List<int> numsAscii = first.codeUnits + second.codeUnits;
      numsAscii.forEach((num e) {
        total += ((e * 65536) * 65536);
      });

      return total;
      
    } catch(e) {
      throw Exception(e);
    }
  }

  static String getOnlyName({ required String path }){
    try {
      String name = path.substring(path.lastIndexOf('/') + 1, path.lastIndexOf('.'));
    
      return name;
      
    } catch(e) {
      throw Exception(e);
    }
  }

  static String getExtension({ required String path }) {
    try {
      
      String extension = path.substring(path.lastIndexOf('.'), path.length);
    
      return extension;
      
    } catch(e) {
      throw Exception(e);
    }
  }

  static String getFilename({ required String path }) {
    try {
    
      String filename = path.substring(path.lastIndexOf('/') + 1, path.length);
    
      return filename;
      
    } catch(e) {
      throw Exception(e);
    }
  }

  static String getPath({ required String path }) {
    try {
      
      String last = path.substring(0, path.lastIndexOf('/'));
    
      return last;
      
    } catch(e) {
      throw Exception(e);
    }
  }

  static String formatNumberToHumanReadable({ required int number, required String languageCode }) {
    try {
      if (number == 0) {
        return '${ number }';
      }

      return NumberFormat.compact(locale: languageCode).format(number);
    } catch(e) {
      throw Exception(e);
    }
  }

  static Route createRoute(Widget page){
    return PageRouteBuilder(pageBuilder: (context, animation, secondaryAnimation) =>
        page,
        transitionsBuilder: (context, animation, secondaryAnimation, child) {
          var begin = Offset(1.0, 0.0);
          var end = Offset.zero;
          var curve = Curves.ease;

          var tween = Tween(begin: begin, end: end).chain(CurveTween(curve: curve));

          return SlideTransition(
            position: animation.drive(tween),
            child: child,
          );
        }
    );
  }

  static Future<void> notify({ required BuildContext context, required String message }) async {
    await Fluttertoast.showToast(msg: message,
      timeInSecForIosWeb: 2,
      gravity: ToastGravity.BOTTOM,
      fontSize: 15,
      textColor: Colors.grey[300],
      backgroundColor: Colors.black.withOpacity(0.75),
    );
    //await Toast.show(message, context, duration: 2, gravity: Toast.BOTTOM, backgroundRadius: 15, textColor: Colors.white, backgroundColor: Colors.black45);
  }

}