
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:bridge/Translate.dart';
import 'package:bridge/domain/model/hashtag/hashtag.dart';
import 'package:bridge/main.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:bridge/shared/custom_field.dart';
import 'package:bridge/shared/loading.dart';
import 'package:bridge/shared/no_result_found.dart';
import 'package:bridge/services/hashtag/hashtag_service.dart';
import 'package:bridge/widgets/search/search_group.dart';
import 'package:bridge/services/utilities/util_service.dart';
import 'package:bridge/widgets/search/user_search.dart';

class SearchScreen extends StatefulWidget {
  @override
  _SearchScreenState createState() => _SearchScreenState();
}

class _SearchScreenState extends State<SearchScreen> {

  final ScrollController _scrollController = ScrollController();
  final TextEditingController _searchEditingController = TextEditingController();
  final HashTagService _hashtagService = HashTagService();

  List<HashTag> _hashtags = [];
  List<HashTag> _search = [];
  late HashTag _hashtag;
  bool _showSearch = false;

  late Translate _intl;

  @override
  void initState() {
    _init();
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
  }

  _init() {
    mainStore.hashtag.addHashtags();
  }

  @override
  Widget build(BuildContext context) {

    _intl = Translate.of(context);

    return AnnotatedRegion<SystemUiOverlayStyle>(
      value: const SystemUiOverlayStyle(
        statusBarColor: Colors.transparent,
        statusBarBrightness: Brightness.dark,
        statusBarIconBrightness: Brightness.dark,
        systemNavigationBarIconBrightness: Brightness.dark,
        systemNavigationBarColor: Colors.black,
        systemNavigationBarDividerColor: Colors.black,
      ),
      child: Material(
        color: Colors.white,
        child: Padding(
          padding: const EdgeInsets.only(left: 18, right: 18, top: 45),
          child: Column(
            children: [
              Column(
                children: [
                  Align(
                    alignment: Alignment.centerLeft,
                    child: Text('${ _intl.translate('SCREEN.SEARCH.TITLE') }', style: TextStyle(fontSize: 25, fontWeight: FontWeight.bold)),
                  ),
                  const SizedBox(height: 18),
                  CustomField(
                    controller: _searchEditingController,
                    autofocus: false,
                    prefixIcon: Icon(Icons.search, color: Colors.grey),
                    suffixIcon: Visibility(
                      visible: _showSearch,
                      child: GestureDetector(
                        onTap: () {
                          _searchEditingController.text = '';
                          setState(() {
                            _search.clear();
                            _showSearch = false;
                          });
                        },
                        child: Icon(Icons.close, size: 25, color: Colors.grey),
                      ),
                    ),
                    text: '${ _intl.translate('SCREEN.SEARCH.FIELD.SEARCH') }',
                    maxLines: 1,
                    backgroundColor: Colors.grey[200],
                    textColor: Colors.grey,
                    borderColor: Colors.grey[200],
                    onChanged: (newName) {
                      _search.clear();

                      if(_showSearch != null && _hashtags.length > 0){
                        if(!newName.isEmpty) {
                          setState(() => _showSearch = true );
                          _hashtags.forEach((elem) {
                            if(elem.name!.startsWith(newName))
                              setState(() => _search.add(elem) );
                          });
                        } else
                          setState(() => _showSearch = false );
                      }

                      /*if(newName.isNotEmpty) {
                        mainStore.hashtag.filterByName(hashtag: HashTag(name: newName));
                     } else
                        mainStore.hashtag.addHashtags();*/
                    },
                  ),
                ],
              ),
              Expanded(
                child: Padding(
                  padding: const EdgeInsets.only(top: 25, bottom: 35), //Menu bottom
                  child: Column(
                    children: [
                      Observer(
                        builder: (_) {

                          return Expanded(
                            child: StreamBuilder(
                              stream: mainStore.hashtag.hashtags,
                              builder: (context, snapshot) {

                                switch(snapshot.connectionState) {
                                  case ConnectionState.none:
                                    return Loading(); //return InternetLoadingFailed();
                                  case ConnectionState.waiting:
                                    return Loading();
                                  case ConnectionState.done:
                                    break;
                                  case ConnectionState.active: {

                                    if(snapshot.hasData) {

                                      final QuerySnapshot query = snapshot.data as QuerySnapshot;

                                      return FutureBuilder(
                                        future: _hashtagService.makeList(snapshot: query),
                                        builder: (context, snapshot) {

                                          if(snapshot.hasData) {

                                            _hashtags = snapshot.data as List<HashTag>;

                                            if(_hashtags.isNotEmpty) {

                                              return NotificationListener<OverscrollIndicatorNotification>(
                                                onNotification: (overscroll) {
                                                  overscroll.disallowIndicator();

                                                  return false;
                                                },
                                                child: ListView.builder(
                                                  itemCount: !_showSearch ? _hashtags.length : _search.length,
                                                  scrollDirection: Axis.vertical,
                                                  cacheExtent: 2000,
                                                  itemBuilder: (context, index) {

                                                    _hashtag = !_showSearch ? _hashtags.elementAt(index) : _search.elementAt(index);

                                                    return SearchGroup(hashtag: _hashtag);
                                                  },
                                                ),
                                              );
                                            }
                                          }

                                          return Container();
                                        },
                                      );
                                    }
                                  } break;
                                }

                                return NoResultFound(icon: Icons.search);
                              },
                            ),
                          );
                        },
                      ),
                      Padding(
                        padding: const EdgeInsets.only(bottom: 38, right: 8),
                        child: Align(
                          alignment: Alignment.centerRight,
                          child: GestureDetector(
                            onTap: () => Navigator.of(context).push(UtilService.createRoute(UserSearch())),
                            child: Container(
                              padding: const EdgeInsets.all(14),
                              decoration: BoxDecoration(
                                color: Colors.black,
                                borderRadius: BorderRadius.circular(100),
                              ),
                              child: const Icon(Icons.person_search, color: Colors.white, size: 25),
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
