
import 'package:bridge/domain/model/media/screen_media_type.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:connectivity/connectivity.dart';
import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:bridge/Translate.dart';
import 'package:bridge/domain/model/media/media.dart';
import 'package:bridge/main.dart';
import 'package:bridge/services/media/media_service.dart';
import 'package:bridge/services/utilities/util_service.dart';
import 'package:bridge/shared/internet_loading_failed.dart';
import 'package:bridge/shared/loading.dart';
import 'package:bridge/shared/message_default.dart';
import 'package:bridge/widgets/media/media_group.dart';

class FollowersMediaPage extends StatefulWidget {
  @override
  _FollowersMediaPageState createState() => _FollowersMediaPageState();
}

class _FollowersMediaPageState extends State<FollowersMediaPage> with AutomaticKeepAliveClientMixin {

  final _mediaService = MediaService();

  late Future<List<Media>> _medias;

  late Translate _intl;

  @override
  void initState() {
    load();
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  void didUpdateWidget(covariant FollowersMediaPage oldWidget) {
    load();
    super.didUpdateWidget(oldWidget);
  }

  load() async {
    await mainStore.recommendation.addFollowingMedias();
  }

  @override
  Widget build(BuildContext context) {

    _intl = Translate.of(context);

    return FutureBuilder(
      future: Future.value(Connectivity().checkConnectivity()),
      builder: (context, snapshot) {

        if(snapshot.hasData) {

          final ConnectivityResult connectivity = snapshot.data as ConnectivityResult;

          if(connectivity != ConnectivityResult.none) {

            return Observer(
              builder: (_) {

                return StreamBuilder(
                  stream: mainStore.recommendation.followingMedias,
                  builder: (context, snapshot) {

                    switch(snapshot.connectionState) {
                      case ConnectionState.none:
                      return InternetLoadingFailed();
                      break;
                      case ConnectionState.waiting:
                      return Loading();
                      break;
                      case ConnectionState.done:
                      return Loading();
                      break;
                      case ConnectionState.active: {

                        if(snapshot.hasData) {

                          final List<QueryDocumentSnapshot> docs = snapshot.data as List<QueryDocumentSnapshot>;

                          return FutureBuilder(
                            future: _mediaService.makeMediaList(docs: docs),
                            builder: (context, snapshot) {
                              if(snapshot.hasData) {

                                final List<Media> audios = snapshot.data as List<Media>;

                                if(audios != null && audios.length > 0) {
                                  return MediaGroup(
                                    type: MediaScreenType.HOME,
                                    keyName: 'following',
                                    medias: audios.toSet(),
                                    isHome: true,
                                    isRecommend: false,
                                  );
                                } else {
                                  return MessageDefault(
                                    title: '${ _intl.translate('COMPONENT.MESSAGE_DEFAULT.FOLLOWING_NO_DATA.TITLE') }',
                                    subtitle: '${ _intl.translate('COMPONENT.MESSAGE_DEFAULT.FOLLOWING_NO_DATA.SUBTITLE') }',
                                    textButton: '${ _intl.translate('COMPONENT.MESSAGE_DEFAULT.FOLLOWING_NO_DATA.BUTTON.LOAD_MEDIA') }',
                                    onTap: () async {
                                      await mainStore.recommendation.addFollowingMedias();

                                      if(!mainStore.recommendation.followingMedias.hasValue ||
                                          mainStore.recommendation.followingMedias.value == null ||
                                          mainStore.recommendation.followingMedias.value.length == 0){
                                        UtilService.notify(context: context, message: '${ _intl.translate('COMPONENT.MESSAGE_DEFAULT.FOLLOWING_NO_DATA.ALERT.NO_DATA') }');
                                      }
                                    },
                                  );
                                }
                              }

                              return Loading();
                            },
                          );
                        }
                      }
                    }

                    return Loading();

                  },
                );
              },
            );
          } else
            return InternetLoadingFailed();
        }

        return Loading();
      },
    );
  }

  @override
  bool get wantKeepAlive => true;
}
