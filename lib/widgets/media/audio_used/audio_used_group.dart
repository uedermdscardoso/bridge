
import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:bridge/domain/model/media/media.dart';
import 'package:bridge/main.dart';
import 'package:bridge/services/media/media_service.dart';
import 'package:bridge/shared/loading.dart';
import 'package:bridge/widgets/media/audio_used/audio_used_content.dart';
import 'package:bridge/widgets/header.dart';

import 'audio_used_data.dart';

//album_complement.dart

class AudioUsedGroup extends StatefulWidget {
  @override
  _AudioUsedGroupGroupState createState() => _AudioUsedGroupGroupState();
}

class _AudioUsedGroupGroupState extends State<AudioUsedGroup> {

  @override
  void initState() {
    super.initState();
  }

  @override
  void dispose() {
    _dispose();
    super.dispose();
  }

  _dispose() {

  }

  @override
  Widget build(BuildContext context) {
    return AnnotatedRegion<SystemUiOverlayStyle>(
      value: const SystemUiOverlayStyle(
        statusBarColor: Colors.transparent,
        statusBarBrightness: Brightness.dark,
        statusBarIconBrightness: Brightness.dark,
        systemNavigationBarColor: Colors.white,
        systemNavigationBarIconBrightness: Brightness.dark,
        systemNavigationBarDividerColor: Colors.white,
      ),
      child: Material(
        color: Colors.white,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            Header(
              color: Colors.transparent,
              hasBorder: true,
            ),
            Expanded(
              child: FutureBuilder(
                future: Future.value(null), //mainStore.studio.backgroundSound,
                builder: (context, snapshot) {

                  if(snapshot.hasData) {

                    final Media media = snapshot.data as Media;

                    return NotificationListener<OverscrollIndicatorNotification>(
                      onNotification: (overscroll) {
                        overscroll.disallowIndicator();

                        return false;
                      },
                      child: NestedScrollView(
                        headerSliverBuilder: (BuildContext context, bool innerBoxIsScrolled) {
                          return <Widget>[
                            SliverAppBar(
                              toolbarHeight: 0,
                              expandedHeight: 200, //260
                              floating: false,
                              pinned: false,
                              backgroundColor: Colors.white,
                              elevation: 0,
                              bottom: const PreferredSize(
                                preferredSize: Size.fromHeight(200), //260
                                child: Text(''),
                              ),
                              flexibleSpace:  AudioUsedData(media: media),
                            ),
                          ];
                        },
                        body: AudioUsedContent(),
                      ),
                    );
                  }

                  return Loading();
                },
              ),
            ),
          ],
        ),
      ),
    );
  }
}
