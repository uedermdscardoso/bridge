
import 'package:flutter/material.dart';
import 'package:flutter/gestures.dart';
import 'package:bridge/domain/model/utilities/report_type.dart';
import 'package:bridge/services/utilities/util_service.dart';
import 'package:bridge/widgets/auth/document_viewer.dart';
import 'package:flutter/services.dart';
import 'package:bridge/domain/model/user/users.dart';
import 'package:bridge/main.dart';
import 'package:bridge/services/user/user_service.dart';
import 'package:bridge/services/utilities/util_service.dart';
import 'package:bridge/shared/custom_field.dart';
import 'package:bridge/widgets/auth/person_name_check.dart';
import 'package:bridge/widgets/header.dart';
import 'package:bridge/widgets/auth/waiting_list_message.dart';
import 'package:bridge/widgets/auth/welcome_message.dart';
import 'package:bridge/domain/model/utilities/login_type.dart';
import 'package:bridge/Translate.dart';

class UsernameCheck extends StatelessWidget {

  final _userService = UserService();
  final _userNameController = TextEditingController();

  final LoginType type;
  final Users? invitedBy;

  double _padding = 0.0;

  UsernameCheck({Key? key,  required this.type, this.invitedBy }) : super(key: key);

  late Translate _intl;

  @override
  Widget build(BuildContext context) {

    _intl = Translate.of(context);

    _padding = MediaQuery.of(context).size.height * 0.057;

    return AnnotatedRegion<SystemUiOverlayStyle>(
      value: SystemUiOverlayStyle(
        statusBarColor: Colors.transparent,
        statusBarBrightness: Brightness.dark,
        statusBarIconBrightness: Brightness.dark,
        systemNavigationBarColor: Colors.grey[100],
        systemNavigationBarIconBrightness: Brightness.dark,
        systemNavigationBarDividerColor: Colors.white,
      ),
      child: Material(
        color: Colors.grey[100],
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            Header(
              color: Colors.transparent,
              hasBorder: false,
            ),
            Expanded(
              child: Padding(
                padding: EdgeInsets.all(_padding), //45
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: [
                    Visibility(
                      visible: this.type.index == LoginType.BY_INVITE.index,
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: [
                          Text('${ _intl.translate('SCREEN.LOGIN.CHECK_USERNAME.MESSAGE.FIRST') } @${ invitedBy != null ? invitedBy!.username : 'unknown' }!',
                              textAlign: TextAlign.start,
                              style: const TextStyle(height: 1.35, fontSize: 20, fontWeight: FontWeight.bold, color: Colors.black)),
                          const SizedBox(height: 12),
                          Text('${ _intl.translate('SCREEN.LOGIN.CHECK_USERNAME.MESSAGE.SECOND') }',
                              style: const TextStyle(height: 1.5, fontSize: 14, fontWeight: FontWeight.bold, color: Colors.grey)),
                        ],
                      ),
                    ),
                    Expanded(
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Text('${ _intl.translate('SCREEN.LOGIN.CHECK_PHONE.FIELD.USERNAME') }',
                              style: const TextStyle(fontSize: 20, fontWeight: FontWeight.bold, color: Colors.black)),
                          const SizedBox(height: 50),
                          Container(
                            width: 260,
                            child: CustomField(
                              style: const TextStyle(color: Colors.black, fontWeight: FontWeight.bold, fontSize: 20),
                              keyboardType: TextInputType.text,
                              prefixIcon: Icon(Icons.alternate_email, size: 22, color: Colors.grey),
                              obscureText: false,
                              controller: _userNameController,
                              textAlign: TextAlign.start,
                              backgroundColor: Colors.white,
                              textColor: Colors.black,
                              borderColor: Colors.white,
                              maxLines: 1,
                              //text: '+55',
                            ),
                          ),
                          const SizedBox(height: 50),
                          Container(
                            width: 260,
                            child: RichText(
                              textAlign: TextAlign.center,
                              text: TextSpan(
                                style: TextStyle(height: 1.25, fontSize: 13, color: Colors.grey),
                                children: <TextSpan>[
                                  TextSpan(text: '${ _intl.translate('SCREEN.LOGIN.CHECK_PHONE.MESSAGE.FIRST_PART_2') } '),
                                  TextSpan(text: '${ _intl.translate('SCREEN.LOGIN.CHECK_PHONE.MESSAGE.TERMS_OF_USE') }',
                                    style: const TextStyle(color: Colors.black, decoration: TextDecoration.underline),
                                    recognizer: TapGestureRecognizer()..onTap = () => _showTermsOfUse(context: context),
                                  ),
                                  TextSpan(text: '${ _intl.translate('SCREEN.LOGIN.CHECK_PHONE.MESSAGE.MIDDLE_PART') }'),
                                  TextSpan(text: '${ _intl.translate('SCREEN.LOGIN.CHECK_PHONE.MESSAGE.PRIVACY_POLICIES') }',
                                    style: const TextStyle(color: Colors.black, decoration: TextDecoration.underline),
                                    recognizer: TapGestureRecognizer()..onTap = () => _showPrivacyPolicy(context: context),
                                  ),
                                  TextSpan(text: '. ${ _intl.translate('SCREEN.LOGIN.CHECK_PHONE.MESSAGE.LAST_PART') }'),
                                ],
                              ),
                            ),
                          ),
                          const SizedBox(height: 50),
                          GestureDetector(
                            onTap: () async => await _save(context: context),
                            child: Container(
                              width: 175,
                              decoration: BoxDecoration(
                                color: Colors.black,
                                borderRadius: BorderRadius.circular(25),
                              ),
                              child: Padding(
                                padding: const EdgeInsets.all(15),
                                child: Text('${ _intl.translate('SCREEN.LOGIN.CHECK_PHONE.ACTION.DONE') }', textAlign: TextAlign.center,
                                    style: const TextStyle(fontSize: 18, color: Colors.white,
                                        fontWeight: FontWeight.bold, decoration: TextDecoration.none)),
                              ),
                            ),
                          ),
                          /*const SizedBox(height: 12),
                          GestureDetector(
                            onTap: () => Navigator.pop(context),
                            child: Container(
                              child: Text('Back', textAlign: TextAlign.center,
                                style: TextStyle(fontSize: 15.5, color: Colors.black54, fontFamily: 'Arial', fontWeight: FontWeight.normal, decoration: TextDecoration.none))
                            ),
                          ),*/
                        ],
                      ),
                    ),
                    //const SizedBox(height: MediaQuery.of(context).viewInsets.bottom * 0.99),
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  _save({ required BuildContext context }) async {
    final String username = _userNameController.text.trim();

    final RegExp validCharacters = RegExp(r'^[a-z0-9_.]+$');

    if(username.isNotEmpty) {
      if(username.length <= 12) { //14

        if(validCharacters.hasMatch(username)) {

          final bool exists = await _userService.existsByUsername(user: Users.onlyUsername(username: username));

          if(!exists) {

            mainStore.auth.addUsername(username: username);

            Navigator.of(context).push(UtilService.createRoute(
              PersonNameCheck(type: type),
            ));

          } else
            UtilService.notify(context: context, message: '${ _intl.translate('SCREEN.LOGIN.CHECK_PHONE.FAILURE.USERNAME_ALREADY_TAKEN') }');
        } else
          UtilService.notify(context: context, message: '${ _intl.translate('SCREEN.LOGIN.CHECK_PHONE.FAILURE.CHARACTERS_NOT_VALID') }');
      } else
        UtilService.notify(context: context, message: '${ _intl.translate('SCREEN.LOGIN.CHECK_PHONE.FAILURE.USERNAME_IS_TOO_LONG') }');
    } else
      UtilService.notify(context: context, message: '${ _intl.translate('SCREEN.LOGIN.CHECK_PHONE.FAILURE.USERNAME_INVALID') }');
  }

  _showPrivacyPolicy({ required BuildContext context }) {
    Navigator.push(context,
      MaterialPageRoute(builder: (context) => const DocumentViewer(
        type: ReportType.PRIVACY_POLICY,
      )),
    );
  }

  _showTermsOfUse({ required BuildContext context }) {
    Navigator.push(context,
      MaterialPageRoute(builder: (context) => const DocumentViewer(
        type: ReportType.TERMS_OF_USE,
      )),
    );
  }

}
