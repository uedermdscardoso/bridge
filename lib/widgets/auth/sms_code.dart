
import 'package:flutter/material.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/services.dart';
import 'package:bridge/domain/model/utilities/report_type.dart';
import 'package:bridge/services/utilities/util_service.dart';
import 'package:bridge/widgets/auth/document_viewer.dart';
import 'package:bridge/widgets/header.dart';
import 'package:bridge/shared/custom_field.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:bridge/widgets/auth/waiting_list_message.dart';
import 'package:bridge/services/user/user_service.dart';
import 'package:bridge/domain/model/user/users.dart';
import 'package:bridge/home.dart';
import 'package:bridge/Translate.dart';
import 'package:bridge/domain/repository/user/user_repository.dart';
import 'package:bridge/services/utilities/util_service.dart';
import 'package:bridge/main.dart';

class SmsCode extends StatelessWidget {

  final _smsCodeController = TextEditingController();

  final String verificationId;

  late Translate _intl;

  SmsCode({ required this.verificationId });

  @override
  Widget build(BuildContext context) {

    _intl = Translate.of(context);

    return AnnotatedRegion<SystemUiOverlayStyle>(
      value: SystemUiOverlayStyle(
        statusBarColor: Colors.transparent,
        statusBarBrightness: Brightness.dark,
        statusBarIconBrightness: Brightness.dark,
        systemNavigationBarColor: Colors.grey[100],
        systemNavigationBarIconBrightness: Brightness.dark,
        systemNavigationBarDividerColor: Colors.white,
      ),
      child: Material(
        color: Colors.grey[100],
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            Header(
              left: Icon(Icons.arrow_back_ios, color: Colors.grey, size: 25),
              color: Colors.transparent,
              hasBorder: false,
            ),
            Expanded(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Text('${ _intl.translate('SCREEN.LOGIN.CHECK_PHONE.FIELD.SMS_CODE') }',
                      style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold, color: Colors.black)),
                  const SizedBox(height: 50),
                  Container(
                    width: 260,
                    child: CustomField(
                      autofocus: false,
                      style: TextStyle(color: Colors.black, fontWeight: FontWeight.bold, fontSize: 20),
                      keyboardType: TextInputType.number,
                      prefixIcon: Icon(Icons.textsms_outlined, size: 25, color: Colors.grey),
                      obscureText: false,
                      controller: _smsCodeController,
                      textAlign: TextAlign.start,
                      backgroundColor: Colors.white,
                      textColor: Colors.black,
                      borderColor: Colors.white,
                      maxLines: 1,
                    ),
                  ),
                  const SizedBox(height: 50),
                  Container(
                    width: 260,
                    child: RichText(
                      textAlign: TextAlign.center,
                      text: TextSpan(
                          style: TextStyle(height: 1.5, fontSize: 13, color: Colors.grey),
                          children: <TextSpan>[
                            TextSpan(text: '${ _intl.translate('SCREEN.LOGIN.CHECK_PHONE.MESSAGE.FIRST_PART') } '),
                            TextSpan(text: '${ _intl.translate('SCREEN.LOGIN.CHECK_PHONE.MESSAGE.TERMS_OF_USE') }',
                              style: TextStyle(color: Colors.black, decoration: TextDecoration.underline),
                              recognizer: new TapGestureRecognizer()..onTap = () => _showTermsOfUse(context: context),
                            ),
                            TextSpan(text: '${ _intl.translate('SCREEN.LOGIN.CHECK_PHONE.MESSAGE.MIDDLE_PART') }'),
                            TextSpan(text: '${ _intl.translate('SCREEN.LOGIN.CHECK_PHONE.MESSAGE.PRIVACY_POLICIES') }',
                              style: TextStyle(color: Colors.black, decoration: TextDecoration.underline),
                              recognizer: new TapGestureRecognizer()..onTap = () => _showPrivacyPolicy(context: context),
                            ),
                            TextSpan(text: '. ${ _intl.translate('SCREEN.LOGIN.CHECK_PHONE.MESSAGE.LAST_PART') }'),
                          ]
                      ),
                    ),
                  ),
                  const SizedBox(height: 50),
                  GestureDetector(
                    onTap: () async => await _verifySmsCode(context: context, verificationId: this.verificationId),
                    child: Container(
                      width: 175,
                      decoration: BoxDecoration(
                        color: Colors.black,
                        borderRadius: BorderRadius.circular(25),
                      ),
                      child: Padding(
                        padding: const EdgeInsets.only(top: 15, bottom: 15, right: 20, left: 30),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Text('${ _intl.translate('SCREEN.LOGIN.CHECK_PHONE.ACTION.NEXT') }', textAlign: TextAlign.center,
                                style: TextStyle(fontSize: 18, color: Colors.white,
                                    fontWeight: FontWeight.bold, decoration: TextDecoration.none)),
                            const Icon(Icons.arrow_forward_ios, color: Colors.white, size: 20),
                          ],
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }

  Future<void> _verifySmsCode({ required BuildContext context, required String verificationId }) async {
    if(_smsCodeController != null && _smsCodeController.text.isNotEmpty) {
      final String smsCode = _smsCodeController.text.trim();

      await authenticate(context: context, smsCode: smsCode, verificationId: verificationId);
    }
  }

  Future<void> authenticate({ required BuildContext context, required String smsCode, required String verificationId }) async {
    try {
      UtilService.onLoading(context: context, message: '${ _intl.translate('SCREEN.LOGIN.CHECK_PHONE.LOADING.CHECKING_SMS_CODE') }');

      final FirebaseAuth auth = FirebaseAuth.instance;
      final PhoneAuthCredential credential = PhoneAuthProvider.credential(
        verificationId: verificationId,
        smsCode: smsCode,
      );
      final UserCredential userCredential = await auth.signInWithCredential(credential);

      if(userCredential != null) {
        final userService = UserService();

        final Users newUser = await userService.getUserWaitingByPhone(phone: userCredential.user!.phoneNumber!);

        if(newUser != null) {
          newUser.id = userCredential.user!.uid;

          final UserRepository userRepository = UserRepository();
          final bool exists = await userRepository.exists(user: newUser);

          if(exists)
            Navigator.of(context).pushAndRemoveUntil(UtilService.createRoute(Home()), (Route<dynamic> route) => false);
          else {
            await mainStore.user.save(user: newUser);

            Navigator.of(context).pushAndRemoveUntil(UtilService.createRoute(Home()), (Route<dynamic> route) => false);
          }
        }
      } else {
        Navigator.of(context).pushAndRemoveUntil(UtilService.createRoute(WaitingListMessage()), (Route<dynamic> route) => false);
      }
    } on FirebaseAuthException catch(e) {
      Navigator.pop(context); //close loading

      if(e.code.compareTo('session-expired') == 0)
        UtilService.notify(context: context, message: '${ _intl.translate('SCREEN.LOGIN.CHECK_PHONE.FAILURE.SMS_CODE_EXPIRED') }');
      else
        UtilService.notify(context: context, message: '${ _intl.translate('SCREEN.LOGIN.CHECK_PHONE.FAILURE.NOT_VERIFY_SMS_CODE') }');
    }
  }

  _showPrivacyPolicy({ required BuildContext context }) {
    Navigator.push(context,
      MaterialPageRoute(builder: (context) => const DocumentViewer(
        type: ReportType.PRIVACY_POLICY,
      )),
    );
  }

  _showTermsOfUse({ required BuildContext context }) {
    Navigator.push(context,
      MaterialPageRoute(builder: (context) => const DocumentViewer(
        type: ReportType.TERMS_OF_USE,
      )),
    );
  }

}
