
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:bridge/Translate.dart';
import 'package:bridge/domain/model/media/media.dart';
import 'package:bridge/domain/model/comment/comment.dart';
import 'package:bridge/main.dart';
import 'package:bridge/services/auth/authentication_service.dart';
import 'package:bridge/services/comment/comment_service.dart';
import 'package:bridge/shared/profile_photo.dart';
import 'package:bridge/shared/multi_modal_dialog.dart';
import 'package:bridge/services/utilities/util_service.dart';

class CommentView extends StatefulWidget {

  Media media;
  Comment comment;

  CommentView({Key? key,  required this.media, required this.comment }) : super(key: key);

  @override
  _CommentViewState createState() => _CommentViewState();
}

class _CommentViewState extends State<CommentView> {

  final String ICON_ACTIONS_ASSET = 'assets/icons/actions';
  final String IMAGE_ASSET = 'assets/images';
  final String ICON_ASSET = 'assets/icons';

  final CommentService _commentService = CommentService();
  final AuthenticationService _authService = AuthenticationService();

  bool _isLiked = false;
  int _totalLikes = 0;
  String copy = '';
  String remove = '';

  bool _buttonPressed = false;
  bool _canRemove = false;
  String _commentedBy = '';

  late Translate _intl;

  @override
  void initState() {
    _loadData();
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
  }

  _loadData() async {
    _commentedBy = widget.comment.author!.id;

    if(!(await _authService.isAnonymous())){
      final currentUser = await _authService.getCurrentUser(); //current User

      if(_commentedBy != null && (currentUser != null && currentUser.id != null))
        _canRemove = _commentedBy.compareTo(currentUser.id) == 0 || _commentedBy.compareTo(currentUser.id) == 0;
    }
  }

  @override
  Widget build(BuildContext context) {

    _intl = Translate.of(context);

    copy = _intl.translate('SCREEN.MEDIA.COMMENT.BUTTON.COPY');
    remove = _intl.translate('SCREEN.MEDIA.COMMENT.BUTTON.REMOVE');

    return ElevatedButton(
      onPressed: () => {},
      onFocusChange: (pressed){
        setState(() {
          _buttonPressed = pressed;
        });
      },
      style: ElevatedButton.styleFrom(
        elevation: 0,
        backgroundColor: Colors.white,
        textStyle: TextStyle(color: _buttonPressed ? Colors.white : Colors.black12),
      ),
      child: Padding(
        padding: const EdgeInsets.only(top: 12, bottom: 12),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Expanded(
              child: GestureDetector(
                onTap: () => _save(context: context, media: widget.media, comment: widget.comment),
                child: Container(
                  color: Colors.transparent,
                  child: Row(
                    children: [
                      ProfilePhoto(width: 30, height: 30, user: widget.comment.author!),
                      const SizedBox(width: 8),
                      Expanded( //Ocupa o meio inteiro
                        child: Container(
                          width: MediaQuery.of(context).size.width * 0.5,
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text(
                                widget.comment.author!.username,
                                style: TextStyle(fontSize: 14, color: Colors.black, decoration: TextDecoration.none, fontWeight: FontWeight.normal, fontFamily: 'Arial'),
                              ),
                              const SizedBox(height: 5),
                              Text(
                                widget.comment.message!,
                                style: TextStyle(fontSize: 14, color: Colors.grey, decoration: TextDecoration.none, fontWeight: FontWeight.normal, fontFamily: 'Arial'),
                              ),
                            ],
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ),
            Column(
              children: [
                FutureBuilder(
                  future: mainStore.comment.isLikedByUser(media: widget.media, comment: widget.comment),
                  builder: (context, snapshot) {

                    if(snapshot.hasData)
                      _isLiked = snapshot.data as bool;

                    return GestureDetector(
                      onTap: () async => await _changeLikeStatus(context: context),
                      child: Image.asset( // 254, 179, 0, 1 - gold
                          '${ ICON_ACTIONS_ASSET }/star-dark.png', //237, 196, 6, 1  yellow
                          color: _isLiked ? const Color.fromRGBO(237, 196, 6, 1) : Color(int.parse("FFE0E0E0", radix: 16)),
                          scale: 1.7),
                    );
                  },
                ),
                const SizedBox(height: 4), //likes
                FutureBuilder(
                  future: mainStore.comment.getTotalLikes(comment: widget.comment),
                  builder: (context, snapshot) {

                    if(snapshot.hasData)
                      _totalLikes = snapshot.data as int;

                    return Text('${ UtilService.formatNumberToHumanReadable(
                        languageCode: 'en',
                        number: _totalLikes
                    ) }', style: const TextStyle(color: Colors.black, fontSize: 12, fontFamily: "Arial", fontWeight: FontWeight.normal, decoration: TextDecoration.none));
                  },
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }

  _save({ required BuildContext context, required Media media, required Comment comment }) {
    showDialog<bool>(
      context: context,
      barrierDismissible: true,
      barrierColor: Colors.white.withAlpha(1),
      builder: (_) {
        String remove = '${ _intl.translate('SCREEN.MEDIA.COMMENT.BUTTON.REMOVE') }';
        String copy = '${ _intl.translate('SCREEN.MEDIA.COMMENT.BUTTON.COPY') }';

        return MultiModalDialog(
          title: '${ _intl.translate('SCREEN.MEDIA.COMMENT.MODAL.REMOVE.TITLE') }',
          values: _canRemove ? [ false, true ] : [ true ],
          buttons: _canRemove ? [ copy, remove ] : [ copy ],
        );
      },
    ).then((isRemoved) async {
      if(isRemoved != null) {
        if(!isRemoved)
          Clipboard.setData(new ClipboardData(text: comment.message));
        else
          await _commentService.removeComment(media: media, comment: comment);
      }
    });
  }

  _changeLikeStatus({ required BuildContext context }) async {
    if(await _authService.isAnonymous())
      UtilService.notify(context: context, message: '${ _intl.translate('SCREEN.MEDIA.COMMENT.ALERT.REGISTER_NOW').toString() }');
    else {
      if(!_isLiked) {
        setState(() => _isLiked = true);

        mainStore.comment.addLike(media: widget.media, comment: widget.comment);
      } else {
        setState(() => _isLiked = false);

        mainStore.comment.removeLike(media: widget.media, comment: widget.comment);
      }
    }
  }

}
