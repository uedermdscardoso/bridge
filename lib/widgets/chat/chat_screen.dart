
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:bridge/Translate.dart';
import 'package:bridge/domain/model/chat/chat.dart';
import 'package:bridge/domain/model/user/users.dart';
import 'package:bridge/main.dart';
import 'package:bridge/services/chat/chat_service.dart';
import 'package:bridge/services/user/user_service.dart';
import 'package:bridge/shared/custom_field.dart';
import 'package:bridge/shared/profile_photo.dart';
import 'package:bridge/widgets/header.dart';
import 'package:bridge/shared/loading.dart';
import 'package:bridge/shared/no_result_found.dart';
import 'package:bridge/shared/internet_loading_failed.dart';

class ChatScreen extends StatefulWidget {

  Users currentUser;
  Users to;

  ChatScreen({Key? key, required this.currentUser, required this.to }) : super(key: key);

  @override
  State<StatefulWidget> createState() => _ChatScreenState();
}

class _ChatScreenState extends State<ChatScreen> with AutomaticKeepAliveClientMixin {

  final _messageTextController = TextEditingController();
  final ChatService _chatService = ChatService();

  late List<Chat> _conversations;
  late Translate _intl;

  @override
  void initState() {
    initValues();
    super.initState();
  }

  @override
  void dispose() {
    mainStore.chat.dispose();
    super.dispose();
  }

  initValues() async {
    await mainStore.chat.addFromAndToById(from: widget.currentUser, to: widget.to);
    await mainStore.chat.addChatByFromAndTo(from: widget.currentUser, to: widget.to);
  }

  @override
  Widget build(BuildContext context) {

    _intl = Translate.of(context);

    return AnnotatedRegion<SystemUiOverlayStyle>(
      value: const SystemUiOverlayStyle(
        statusBarColor: Colors.transparent,
        statusBarBrightness: Brightness.dark,
        statusBarIconBrightness: Brightness.dark,
        systemNavigationBarIconBrightness: Brightness.dark,
        systemNavigationBarColor: Colors.white,
        systemNavigationBarDividerColor: Colors.white,
      ),
      child: Material(
        color: Colors.white,
        child: Observer(
          name: 'chat',
          builder: (_) {

            Users from = mainStore.chat.from;
            Users to = mainStore.chat.to;

            return Column(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [ //Color.fromRGBO(246, 246, 246, 1)
                Header(
                  color: Colors.transparent,
                  left: Visibility(
                    visible: to != null && to.firstName != null && to.lastName != null,
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: [
                        GestureDetector(
                          onTap: () => Navigator.pop(context),
                          child: Container(
                            color: Colors.transparent,
                            child: const Icon(Icons.arrow_back_ios, color: Colors.grey, size: 25),
                          ),
                        ),
                        const SizedBox(width: 6),
                        Text('${ to.firstName }', style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold)),
                        Text(to.lastName, style: const TextStyle(fontSize: 18, fontWeight: FontWeight.bold)),
                      ],
                    ),
                    replacement: Text('${ to.username } ', style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold)),
                  ),
                  hasBorder: true,
                ),
                Expanded(
                  child: StreamBuilder(
                    stream: mainStore.chat.chat, //FirebaseFirestore.instance.collection('conversations').snapshots(),
                    builder: (context, snapshot){

                      switch(snapshot.connectionState) {
                        case ConnectionState.none:
                          return NoResultFound(icon: Icons.chat_bubble_outline);
                          //return InternetLoadingFailed();
                          break;
                        case ConnectionState.waiting:
                          return Loading();
                          break;
                        case ConnectionState.done:
                          break;
                        case ConnectionState.active: {

                          if(snapshot.hasData) {

                            final QuerySnapshot query = snapshot.data as QuerySnapshot;

                            return FutureBuilder(
                              future: _chatService.makeList(data: query),
                              builder: (contextChat, snapshotChat) {

                                if(snapshotChat.hasData) {

                                  _conversations = snapshotChat.data as List<Chat>;

                                  if(_conversations.isNotEmpty) {

                                    return NotificationListener<OverscrollIndicatorNotification>(
                                      onNotification: (overscroll) {
                                        overscroll.disallowIndicator();

                                        return false;
                                      },
                                      child: ListView.builder(
                                        reverse: true,
                                        itemCount: _conversations.length,
                                        physics: const PageScrollPhysics(),
                                        scrollDirection: Axis.vertical,
                                        padding: EdgeInsets.all(0),
                                        itemBuilder: (context, position) {

                                          String fromId = _conversations.elementAt(position).from;
                                          //DateTime createdIn = DateTime.fromMillisecondsSinceEpoch(conversations.elementAt(position).createdIn);

                                          return Padding(
                                            padding: const EdgeInsets.all(12),
                                            child: widget.currentUser.id.compareTo(fromId) == 0 ?
                                            showMessageByFrom(from: from, conversations: _conversations, position: position) :
                                            showMessageByTo(to: to, conversations: _conversations, position: position),
                                          );
                                        },
                                      ),
                                    );
                                  }
                                }

                                return Container();
                                //return NoResultFound(icon: Icons.chat_bubble_outline);
                              },
                            );
                          }

                          return Container();
                        } break;
                      }

                      return Container();
                    },
                  ),
                ),
                Container(
                  color: Colors.white,
                  width: double.infinity,
                  height: 65,
                  child: Center(
                      child: Row(
                        children: [
                          Container(
                            width: MediaQuery.of(context).size.width-45,
                            child: CustomField(
                                obscureText: false,
                                controller: _messageTextController,
                                textAlign: TextAlign.start,
                                backgroundColor: Colors.white,
                                textColor: Colors.grey,
                                borderColor: Colors.white,
                                text: '${ _intl.translate('SCREEN.NOTIFICATION.CHAT.FIELD.MESSAGE') }'
                            ),
                          ),
                          GestureDetector(
                              onTap: () => {
                                _chatService.createChat(
                                  chat: Chat(
                                      from: widget.currentUser.id,
                                      to: widget.to.id,
                                      createdIn: DateTime.now().millisecondsSinceEpoch,
                                      message: _messageTextController.text
                                  ),
                                ),
                                _messageTextController.clear(),
                              },
                              child: Icon(Icons.play_arrow, color: Colors.grey, size: 30)
                          ),
                        ],
                      )
                  ),
                ),
                SizedBox(height: MediaQuery.of(context).viewInsets.bottom * 0.99), // When open keyboard
              ],
            );
          },
        ),
      ),
    );
  }

  Widget showMessageByTo({ required Users to, required List<Chat> conversations, required int position }){
    return Row(
      mainAxisAlignment: MainAxisAlignment.start,
      children: [
        ProfilePhoto(user: to, width: 40, height: 40),
        const SizedBox(width: 5),
        Flexible(
          child: Container(
            decoration: BoxDecoration(
              color: Color.fromRGBO(250,250,210, 1), //Color.fromRGBO(224, 224, 224, 1),
              borderRadius: BorderRadius.circular(12),
            ),
            child: Padding(
              padding: const EdgeInsets.only(top: 12, bottom: 12, left: 22, right: 22),
              child: Text(
                conversations.elementAt(position).message,
                style: const TextStyle(
                  fontSize: 15,
                  color: Colors.black,
                  decoration: TextDecoration.none,
                  fontWeight: FontWeight.normal,
                  fontFamily: 'Arial',
                ),
              ),
            ),
          ),
        ),
      ],
    );
  }

  Widget showMessageByFrom({ required Users from, required List<Chat> conversations, required int position }){

    return Row(
      mainAxisAlignment: MainAxisAlignment.end,
      children: [
        Flexible(
          child: Container(
            decoration: BoxDecoration(
              color: Color.fromRGBO(240, 240, 240, 1), //Colors.lightGreen[100],
              borderRadius: BorderRadius.circular(12),
            ),
            child: Padding(
              padding: const EdgeInsets.only(top: 12, bottom: 12, left: 22, right: 22),
              child: Text(
                conversations.elementAt(position).message,
                style: TextStyle(
                  fontSize: 15,
                  color: Colors.black,
                  decoration: TextDecoration.none,
                  fontWeight: FontWeight.normal,
                  fontFamily: 'Arial',
                ),
              ),
            ),
          ),
        ),
        const SizedBox(width: 5),
        ProfilePhoto(user: from, width: 40, height: 40),
      ],
    );
  }

  @override
  bool get wantKeepAlive => true;
}
