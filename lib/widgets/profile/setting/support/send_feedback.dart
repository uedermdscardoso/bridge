
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:bridge/domain/model/user/users.dart';
import 'package:bridge/services/utilities/util_service.dart';
import 'package:bridge/shared/custom_field.dart';
import 'package:bridge/widgets/header.dart';
import 'package:bridge/domain/model/feedback/feedbacks.dart';
import 'package:bridge/services/feedback/feedback_service.dart';
import 'package:bridge/Translate.dart';

class SendFeedback extends StatefulWidget {

  final Users currentUser;

  const SendFeedback({Key? key,  required this.currentUser }) : super(key: key);

  @override
  _SendFeedbackState createState() => _SendFeedbackState();

}

class _SendFeedbackState extends State<SendFeedback> {

  final TextEditingController _messageController = TextEditingController();

  final FeedbackService _feedbackService = FeedbackService();

  late Translate _intl;

  @override
  void initState() {
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {

    _intl = Translate.of(context);

    return AnnotatedRegion<SystemUiOverlayStyle>(
      value: SystemUiOverlayStyle(
        statusBarColor: Colors.transparent,
        statusBarBrightness: Brightness.dark,
        statusBarIconBrightness: Brightness.dark,
        systemNavigationBarColor: Colors.white,
        systemNavigationBarIconBrightness: Brightness.dark,
        systemNavigationBarDividerColor: Colors.white,
      ),
      child: Material(
        color: Colors.white,
        child: Column(
          children: [
            Header(
              color: Colors.transparent,
              left: Row(
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  GestureDetector(
                    onTap: () => Navigator.pop(context),
                    child: Container(
                      child: Icon(Icons.arrow_back_ios, color: Colors.grey, size: 25),
                    ),
                  ),
                  const SizedBox(width: 4),
                  Text('${ _intl.translate('SCREEN.SETTING.FEEDBACK.TITLE') }', style: TextStyle(color: Colors.black, fontWeight: FontWeight.bold, fontSize: 18)),
                ],
              ),
              right: GestureDetector(
                onTap: () async => _sendFeedback(context: context),
                child: Padding(
                  padding: const EdgeInsets.only(top: 5, bottom: 5, right: 8, left: 8),
                    child: Text('${ _intl.translate('SCREEN.SETTING.FEEDBACK.BUTTON.SEND') }', style: TextStyle(color: Colors.black, decoration: TextDecoration.none, fontSize: 16, fontWeight: FontWeight.normal, fontFamily: 'Arial')),
                ),
              ),
              hasBorder: true,
            ),
            Expanded(
              child: Padding(
                padding: const EdgeInsets.all(16),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: [
                    Padding(
                      padding: const EdgeInsets.only(top: 12, left: 8, right: 8, bottom: 8),
                      child: Align(
                        alignment: Alignment.topLeft,
                        child: Text('${ _intl.translate('SCREEN.SETTING.FEEDBACK.TIP') }', style: TextStyle(fontSize: 12, color: Colors.grey)),
                      ),
                    ),
                    CustomField(
                      controller: _messageController,
                      maxLines: 8,
                      autofocus: false,
                      obscureText: false,
                      textAlign: TextAlign.start,
                      backgroundColor: Colors.white,
                      textColor: Colors.grey,
                      borderColor: Colors.grey[300],
                      text: '${ _intl.translate('SCREEN.SETTING.FEEDBACK.FIELD.MESSAGE') }',
                    ),
                  ],
                ),
              ),
            )
          ],
        ),
      ),
    );
  }

  _sendFeedback({ required BuildContext context }) async {
    if(_messageController.text.isNotEmpty) {
      final Feedbacks feedback = Feedbacks(message: _messageController.text, sentBy: widget.currentUser);

      await _feedbackService.send(feedback: feedback);

      UtilService.notify(context: context, message: '${ _intl.translate('SCREEN.SETTING.FEEDBACK.NOTIFICATION.SUCCESS') }');

      Navigator.pop(context);
    }
  }
}
